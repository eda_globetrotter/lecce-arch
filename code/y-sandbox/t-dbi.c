/**
 * This is written by Zhiyang Ong to test the struct dbi.
 *
 * This tests the functionality of the DBI.
 *
 *
 *	Synopsis:
 *		Test the allocation of memory to the DBI.
 *		Test if I can print the contents of the DBI.
 *		
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * To ensure that the results of the automated regression testing
 *	are repeatable and replicable, set the parameters in cache.h
 *	according to the following.
 * #define CACHE_BLK_VALID		0x00000001
 * #define CACHE_BLK_DIRTY		0x00000002
 * #define DBI_SIZE								6
 * #define DBI_GRANULARITY						9
 * #define LEAST_RECENTLY_WRITTEN				0
 * #define LRW_W_BIMODAL_INSERTION				1
 * #define REWRITE_INTERVAL_PREDICTION			2
 * #define MAX_ENTRY							3
 * #define MIN_ENTRY							4
 * #define DBI_CACHE_REPLACEMENT_POLICY			LEAST_RECENTLY_WRITTEN
 *
 *
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */



/**
 * Modified by Zhiyang Ong
 * Include Standard C Library
 */
#include <stdlib.h>
#include <stdio.h>
/* Module/Unit under test: DBI */
#include "dbi.h"


int main()
{
	/* =============================================== */

	int i,j;

	/* Test if DBI and DBI entry implementation works */
	struct dirtyBlockIndex *dbiObject;
	/* Allocate memory space to it. */
/*
	dbiObject.dbi = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*DBI_SIZE);
	printf("==	Allocated memory to DBI.\n");
	
	for(i=0; i<DBI_SIZE; i++) {
		dbiObject.dbi[i].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*DBI_GRANULARITY);
	}
*/
	dbiAllocateMemory(dbiObject);
	
	/* =============================================== */
	/* Print the contents of the DBI. */
	/* dbiToString(*dbiObject); */
	
	/* =============================================== */
	/* Add valid DBI entries to the DBI. */
	
	/* For each DBI entry in the DBI. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Assign values to the DBI entry. */
		dbiObject->dbi[i].validStatus = i*10+1;
		dbiObject->dbi[i].rowTag = i + 200;
		// Populate the dirty-bit vector.
		for(j=0; j<DBI_GRANULARITY; j++) {
			dbiObject->dbi[i].dirtyBitVector[j] = j%2;
		}
		dbiObject->dbi[i].nthWriteOperation = i*i+500;
	}
printf("Pre-Modification of DBV of DBI entry #4.\n");



	/* ============================================================ */
	/* Testing DBI replacement policy. */

	/* Set the dirty-bit vector of dbi[3] to: 101101110. Total = 6. */
	dbiObject->dbi[3].dirtyBitVector[0] = false;
	dbiObject->dbi[3].dirtyBitVector[1] = true;
	dbiObject->dbi[3].dirtyBitVector[2] = true;
	dbiObject->dbi[3].dirtyBitVector[3] = true;
	dbiObject->dbi[3].dirtyBitVector[4] = false;
	dbiObject->dbi[3].dirtyBitVector[5] = true;
	dbiObject->dbi[3].dirtyBitVector[6] = true;
	dbiObject->dbi[3].dirtyBitVector[7] = false;
	dbiObject->dbi[3].dirtyBitVector[8] = true;
printf("Modified DBV of DBI entry #4.\n");
	/* -------------------------------------------------------------*/
	
	/* Set the dirty-bit vector of dbi[0] to: 000001110. Total = 3. */
	dbiObject->dbi[0].dirtyBitVector[0] = false;
	dbiObject->dbi[0].dirtyBitVector[1] = true;
	dbiObject->dbi[0].dirtyBitVector[2] = true;
	dbiObject->dbi[0].dirtyBitVector[3] = true;
	dbiObject->dbi[0].dirtyBitVector[4] = false;
	dbiObject->dbi[0].dirtyBitVector[5] = false;
	dbiObject->dbi[0].dirtyBitVector[6] = false;
	dbiObject->dbi[0].dirtyBitVector[7] = false;
	dbiObject->dbi[0].dirtyBitVector[8] = false;
	
	/* Set the dirty-bit vector of dbi[1] to: 001000000. Total = 1. */
	dbiObject->dbi[1].dirtyBitVector[0] = false;
	dbiObject->dbi[1].dirtyBitVector[1] = false;
	dbiObject->dbi[1].dirtyBitVector[2] = false;
	dbiObject->dbi[1].dirtyBitVector[3] = false;
	dbiObject->dbi[1].dirtyBitVector[4] = false;
	dbiObject->dbi[1].dirtyBitVector[5] = false;
	dbiObject->dbi[1].dirtyBitVector[6] = true;
	dbiObject->dbi[1].dirtyBitVector[7] = false;
	dbiObject->dbi[1].dirtyBitVector[8] = false;
printf("Modified DBV of DBI entry #1.\n");
	/* Set the dirty-bit vector of dbi[2] to: 010000010. Total = 2. */
	dbiObject->dbi[2].dirtyBitVector[0] = false;
	dbiObject->dbi[2].dirtyBitVector[1] = true;
	dbiObject->dbi[2].dirtyBitVector[2] = false;
	dbiObject->dbi[2].dirtyBitVector[3] = false;
	dbiObject->dbi[2].dirtyBitVector[4] = false;
	dbiObject->dbi[2].dirtyBitVector[5] = false;
	dbiObject->dbi[2].dirtyBitVector[6] = false;
	dbiObject->dbi[2].dirtyBitVector[7] = true;
	dbiObject->dbi[2].dirtyBitVector[8] = false;
	
	/* -------------------------------------------------------------*/
	
	/* Set the dirty-bit vector of dbi[4] to: 010000010. Total = 2. */
	dbiObject->dbi[4].dirtyBitVector[0] = false;
	dbiObject->dbi[4].dirtyBitVector[1] = true;
	dbiObject->dbi[4].dirtyBitVector[2] = false;
	dbiObject->dbi[4].dirtyBitVector[3] = false;
	dbiObject->dbi[4].dirtyBitVector[4] = false;
	dbiObject->dbi[4].dirtyBitVector[5] = false;
	dbiObject->dbi[4].dirtyBitVector[6] = false;
	dbiObject->dbi[4].dirtyBitVector[7] = true;
	dbiObject->dbi[4].dirtyBitVector[8] = false;
	
	/* Set the dirty-bit vector of dbi[5] to: 010000010. Total = 5. */
	dbiObject->dbi[5].dirtyBitVector[0] = true;
	dbiObject->dbi[5].dirtyBitVector[1] = true;
	dbiObject->dbi[5].dirtyBitVector[2] = false;
	dbiObject->dbi[5].dirtyBitVector[3] = false;
	dbiObject->dbi[5].dirtyBitVector[4] = true;
	dbiObject->dbi[5].dirtyBitVector[5] = false;
	dbiObject->dbi[5].dirtyBitVector[6] = true;
	dbiObject->dbi[5].dirtyBitVector[7] = true;
	dbiObject->dbi[5].dirtyBitVector[8] = false;
/* printf("Modified DBV of DBI entry #5.\n"); */
	/* Set the dirty-bit vector of dbi[6] to: 010000010. Total = 1. */
/*
	dbiObject->dbi[6].dirtyBitVector[0] = false;
	dbiObject->dbi[6].dirtyBitVector[1] = true;
	dbiObject->dbi[6].dirtyBitVector[2] = false;
	dbiObject->dbi[6].dirtyBitVector[3] = false;
	dbiObject->dbi[6].dirtyBitVector[4] = false;
	dbiObject->dbi[6].dirtyBitVector[5] = false;
	dbiObject->dbi[6].dirtyBitVector[6] = false;
	dbiObject->dbi[6].dirtyBitVector[7] = false;
	dbiObject->dbi[6].dirtyBitVector[8] = false;
*/
	/* Set the dirty-bit vector of dbi[7] to: 010000010. Total = 4. */
/*
	dbiObject->dbi[7].dirtyBitVector[0] = false;
	dbiObject->dbi[7].dirtyBitVector[1] = false;
	dbiObject->dbi[7].dirtyBitVector[2] = false;
	dbiObject->dbi[7].dirtyBitVector[3] = false;
	dbiObject->dbi[7].dirtyBitVector[4] = true;
	dbiObject->dbi[7].dirtyBitVector[5] = true;
	dbiObject->dbi[7].dirtyBitVector[6] = false;
	dbiObject->dbi[7].dirtyBitVector[7] = true;
	dbiObject->dbi[7].dirtyBitVector[8] = true;
*/
	/* Set the dirty-bit vector of dbi[8] to: 010000010. Total = 3. */
/*
	dbiObject->dbi[8].dirtyBitVector[0] = false;
	dbiObject->dbi[8].dirtyBitVector[1] = false;
	dbiObject->dbi[8].dirtyBitVector[2] = false;
	dbiObject->dbi[8].dirtyBitVector[3] = false;
	dbiObject->dbi[8].dirtyBitVector[4] = true;
	dbiObject->dbi[8].dirtyBitVector[5] = true;
	dbiObject->dbi[8].dirtyBitVector[6] = false;
	dbiObject->dbi[8].dirtyBitVector[7] = true;
	dbiObject->dbi[8].dirtyBitVector[8] = false;
printf("Modified DBV of DBI entry #8.\n");
*/	

/* printf("Pre-Modification of DBV of replacement DBI entry.\n"); */
	/* DBI entry to replace the evicted DBI entry. */
	struct dirtyBlockIndexEntry replacementDBI;
	replacementDBI.dirtyBitVector = (bool *)malloc(sizeof(bool)*DBI_GRANULARITY);
	replacementDBI.validStatus = 123;
	replacementDBI.rowTag = 500;
	replacementDBI.dirtyBitVector[0] = true;
	replacementDBI.dirtyBitVector[1] = true;
	replacementDBI.dirtyBitVector[2] = false;
	replacementDBI.dirtyBitVector[3] = true;
	replacementDBI.dirtyBitVector[4] = true;
	replacementDBI.dirtyBitVector[5] = false;
	replacementDBI.dirtyBitVector[6] = true;
/* printf("Modified DBV of replacement DBI entry	6.\n");*/
	replacementDBI.dirtyBitVector[7] = true;
	replacementDBI.dirtyBitVector[8] = true;
	replacementDBI.nthWriteOperation = 13;
	
	
	
	
	
printf("Modified DBV of replacement DBI entry.\n");
	
	/* =============================================== */
	/* Print the contents of the DBI. */
	dbiToString(*dbiObject);
/* printf("DBI toString() is done.\n"); */
/**
NUM_DBI_ROWS_USED++;
NUM_DBI_ROWS_USED++;
NUM_DBI_ROWS_USED++;
printf("NUM_DBI_ROWS_USED is:%i.\n", NUM_DBI_ROWS_USED);
**/

printf("Test the max dirty replacement policy.\n\n\n\n\n\n");
	/* Test the max dirty replacement policy. */
	/*dbiMaxDirtyPolicy(*dbiObject,replacementDBI);*/
	/* DBI_CACHE_REPLACEMENT_POLICY = MAX_ENTRY; */
printf("DBI_CACHE_REPLACEMENT_POLICY (MAX???) is:%i\n",DBI_CACHE_REPLACEMENT_POLICY);
	specifiedDbiReplacementPolicy(*dbiObject,replacementDBI, MAX_ENTRY);
	
	/* dbiEntryToString(dbiObject->dbi[3]); */
	dbiToString(*dbiObject);

printf("Test the min dirty replacement policy.\n\n\n\n\n\n");
	/* Test the min dirty replacement policy. */
	replacementDBI.validStatus = 456;
	replacementDBI.rowTag = 900;
	replacementDBI.dirtyBitVector[0] = false;
	replacementDBI.dirtyBitVector[1] = false;
	replacementDBI.dirtyBitVector[2] = false;
	replacementDBI.dirtyBitVector[3] = true;
	replacementDBI.dirtyBitVector[4] = true;
	replacementDBI.dirtyBitVector[5] = false;
	replacementDBI.dirtyBitVector[6] = true;
/* printf("Modified DBV of replacement DBI entry	6.\n");*/
	replacementDBI.dirtyBitVector[7] = true;
	replacementDBI.dirtyBitVector[8] = false;
	replacementDBI.nthWriteOperation = 14;
	
	/* DBI_CACHE_REPLACEMENT_POLICY = MIN_ENTRY; */
	specifiedDbiReplacementPolicy(*dbiObject,replacementDBI, MIN_ENTRY);
	/* dbiMinDirtyPolicy(*dbiObject,replacementDBI); */
	dbiToString(*dbiObject);
	
	
	
	
	printf("Test the LRW dirty replacement policy.\n\n\n\n\n\n");
	/* Test the min dirty replacement policy. */
	replacementDBI.validStatus = 789;
	replacementDBI.rowTag = 1100;
	replacementDBI.dirtyBitVector[0] = true;
	replacementDBI.dirtyBitVector[1] = false;
	replacementDBI.dirtyBitVector[2] = true;
	replacementDBI.dirtyBitVector[3] = false;
	replacementDBI.dirtyBitVector[4] = true;
	replacementDBI.dirtyBitVector[5] = true;
	replacementDBI.dirtyBitVector[6] = true;
/* printf("Modified DBV of replacement DBI entry	6.\n");*/
	replacementDBI.dirtyBitVector[7] = false;
	replacementDBI.dirtyBitVector[8] = false;
	replacementDBI.nthWriteOperation = 15;
	
	dbiObject->dbi[0].nthWriteOperation = 11;
	dbiObject->dbi[1].nthWriteOperation = 10;
	dbiObject->dbi[2].nthWriteOperation = 13;
	dbiObject->dbi[3].nthWriteOperation = 14;
	dbiObject->dbi[4].nthWriteOperation = 9;
	dbiObject->dbi[5].nthWriteOperation = 12;
	
	/*DBI_CACHE_REPLACEMENT_POLICY = LEAST_RECENTLY_WRITTEN */;
	specifiedDbiReplacementPolicy(*dbiObject,replacementDBI, LEAST_RECENTLY_WRITTEN);
	/* dbiLeastRecentlyWritten(*dbiObject,replacementDBI); */
	dbiToString(*dbiObject);
	

	NUMBER_OF_WRITES_TO_DBI_ENTRIES = 7;
	if(7 == NUMBER_OF_WRITES_TO_DBI_ENTRIES) {
		printf(">	>	NUMBER_OF_WRITES_TO_DBI_ENTRIES:%i.\n",NUMBER_OF_WRITES_TO_DBI_ENTRIES);
	}
	
	
	
	
	return 0;
}






















