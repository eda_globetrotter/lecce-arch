/*
########################################################################   
# Program Name: dbiIndex.c										       #
# Author: Anthony C. Rivera Carvelli                                   #
# Date: 12/3/2014                                                      #
#                                                                      #
# Info: This is a proof of concept to get the dirty block index working#
# on SimpleScalar. Compile this program on the simple scalar main      #
# directory to get it working because it uses a library from           #
# SimpleScalar. The tag store structure is not the same one used on    #
# SimpleScalar and was provided just as a proof of concept. I will     #
# implement it with the original one later on                          #
########################################################################

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Modified by Zhiyang Ong
 * Include Standard C Library
 */
#include <stdlib.h>
#include <stdio.h>


/* Include the Header file for our code. */
#include "dbiEntry.h"

int main()
{
	/* Creating a Dirty-Block Index (DBI) for the processor. */
	struct dirtyBlockIndexEntry *dbi;
	/* Assigning values to the parameters of DBI */
	int dbiSize = 50;
	int dbiGranularity = 50;
	int count;
	/**
	 * Allocating memory space for DBI
	 * allocate space for dirtyBlockIndexEntry array called dbi
	 */
	dbi = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*dbiSize);
	/**
	 * Allocating memory space for the dirty-bit vector in each
	 *	DBI entry. 
	 */
	for (count = 0; count < dbiSize; count++)
	{
		//allocate space for dirtyBitVector (array) on each struct on the array
		dbi[count].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*dbiGranularity);  
	}
	
	/* ======================================================== */

	/**
	 * Modified by Zhiyang Ong
	 * Determine if I can populate and access the contents of
	 *	a DBI entry.
	 */









	/* Trash the following. */

	/**
	 * Modified by Zhiyang Ong
	 * Creat a DBI entry.
	 * Insert DBI entry into DBI.
	 * Check if values of DBI entry is assigned correctly.
	 */
	struct dirtyBlockIndexEntry b;
	b.validStatus = CACHE_BLK_DIRTY;
	b.validStatus = 79;
	b.rowTag = 287;
	printf("==	dirtyBlockIndexEntry.validStatus is:%i.\n", b.validStatus);
	printf("==	dirtyBlockIndexEntry.rowTag is:%i.\n", b.rowTag);
	printf(">	Printed valid status\n");
	dbi[0] = b;
	printf("==	dbi[0].validStatus is:%i.\n", dbi[0].validStatus);
	printf("==	dbi[0].rowTag is:%i.\n", dbi[0].rowTag);
	int bit_vector_size = 7;
	/* Allocate space for dirty-bit vector */
	b.dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*bit_vector_size);
	
	int i,j;
printf(">	Entering the outer for loop\n");
	for(j=0; j<5; j++) {
		b.dirtyBitVector[j] = j + 150;
		printf("Ciao Mondo:%i.\n", b.dirtyBitVector[j]);
	}


	/* =============================================== */

	/* Test if DBI and DBI entry implementation works */
	int c_size = 6;
	int bv_size = 7;
	/* Create a DBI of size 5. */
	struct dirtyBlockIndexEntry *c;
	/* Allocate memory space to it. */
	c = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*c_size);
	/* Allocate memory space to the dirty-bit vector of each DBI entry*/
	for(i=0; i<c_size; i++) {
		c[i].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*bv_size);
	}

	/* For each DBI entry in the DBI. */
	for(i=0; i<c_size; i++) {
		/* Assign values to the DBI entry. */
		c[i].validStatus = i*10+1;
		c[i].rowTag = i + 200;
		// Populate the dirty-bit vector.
		for(j=0; j<bv_size; j++) {
			c[i].dirtyBitVector[j] = j + 150;
		}
	}
	
	/* For each DBI entry in the DBI. */
	for(i=0; i<c_size; i++) {
		/* Read values from the DBI entry. */
		printf("	c[i].validStatus:%i.\n", c[i].validStatus);
		printf("	c[i].rowTag:%i.\n", c[i].rowTag);
		for(j=0; j<bv_size; j++) {
			printf("	c[i].validStatus[j]:%i.\n", c[i].dirtyBitVector[j]);
		}
	}

	return 0;
}
