#!/usr/bin/python

"""
	This is written by Zhiyang Ong to obtain key information
		from the gem5 simulation results.
 
	Synopsis:
	Script to obtain key information from gem5 simulation results.
	
	Procedure:
	For each benchmark,
		1)	Access simulation output files containing simulation
				results
		2)	Process each simulation output file
				Carry out statistical analysis on that file.		
	
		3) Obtain statistical values and update the list with
				that information.



	
	
			
	References:
	Adam Pierce and Matthew Eager (Editor), Answer to "How do
		I modify a text file in Python?", in Stack Overflow,
		Stack Exchange Inc., New York, NY, September 24, 2008
		(Modified October 9, 2012).

	Revision History:
	1)	December 5, 2014. Initial working version.
	2)	???
 
 
 
 	The MIT License (MIT)
 
 	Copyright (c) <2014> <Zhiyang Ong>
 
 	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 	Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"
"""

#	Enable floating-point division of 2 integers
from __future__ import division

#	Import packages and functions from the Python Standard Library.
import os.path, string, sys
from os.path import isfile
from os import remove
from string import find
from shutil import move
#	= For making system calls
from subprocess import call
#	= For determining exponentiation
from math import pow
#import math


#	Import my Python modules
#from utilities import utilities

"""
	Use the default functions for:
		Sum of numbers			sum
		Maximum value			max
		Minimum value			min
		
	
	See https://docs.python.org/2/library/functions.html.
	
	Reference:
	Fred L. Drake, Jr. et al., "Section 2. Built-in Functions", in Python 2.7.9rc1 documentation, Python Software Foundation, Beaverton, OR, Dec 7, 2014. Available as: https://docs.python.org/2/library/functions.html; last view on December 8, 2014.
	Also, Section 9.2 math -- Mathematical functions,
		https://docs.python.org/2/library/math.html
"""


#	============================================================

"""
	Configuration parameters of the program to be fixed by users.

	#	absolute/relative path to directory of benchmarks
	#	benchmarks to look at
	#	filenames for each benchmark to look at
	#	keywords/keyphrases for important metrics in each
			important simulation file.
	#	name of output file containing the results
	
"""



"""
	Relative path to the location of benchmarks.
	
	This relative path is the directory contain subdirectories
		for benchmarks.
	That is, each benchmark, its associated input/output files
		are located in a subdirectory named after the benchmark.
	
	For example, if the current directory has subdirectories
		bmk1, bmk2, and bmk3 that contain simulation results
		for benchmarks bmk1, bmk2, and bmk3, the relative path
		of the current directory shall be provided as the
		relative path 
"""
#location_results = "/scratch/grad/zhiyang/romagna-stat/676_result_baseline_16_64/676_m5out_baseline_20141031"
location_results = "/scratch/grad/zhiyang/romagna-stat/676_result_baseline_16_64/676_m5out_baseline_20141031"



#	Names of benchmarks from the PARSEC benchmark suite
bmk_names = ["blackscholes", "bodytrack", "canneal", "dedup", "ferret", "fluidanimate", "freqmine", "streamcluster", "swaptions", "vips", "x264"]

#	Names of gem5 output files containing simulation results.
sim_op_files = ["stats.txt","ruby.stats"]
#	Index to "stats.txt" in sim_op_files.
st_in_sof = 0
#	Index to "ruby.stats" in sim_op_files.
rs_in_sof = 1


#	Key terms for the simulation output file: stats.txt
stats_txt_keyterms = ["sim_seconds", "host_seconds", "percent_links_utilized", "rename.LSQFullEvents", "lsq.thread0.cacheBlocked", "ipc_total", "L1Dcache.demand_hits", "L1Dcache.demand_misses", "L1Dcache.demand_accesses", "L2cache.demand_hits", "L2cache.demand_misses", "L2cache.demand_accesses"]
list_sim_seconds = []
list_host_seconds = []
list_percent_links_utilized = []
list_rename_LSQFullEvents = []
list_lsq_thread0_cacheBlocked = []
list_ipc_total = []
list_L1Dcache_demand_hits = []
list_L1Dcache_demand_misses = []
list_L1Dcache_demand_accesses = []
list_L2cache_demand_hits = []
list_L2cache_demand_misses = []
list_L2cache_demand_accesses = []

"""
	Additional notes:
	Metrics that repeat 4 times:
		"sim_seconds"
		"host_seconds"
	
	
	Regarding "percent_links_utilized", there exists 34 routers
		in the "stats.txt" file that Javier sent me.
	Implement two versions for processing this metric to process
		all values and only 2^n values.
	I note that the last 2 "Router #32" have values of 0.
	
	
	Metrics obtained for each core:
		"rename.LSQFullEvents"
		"lsq.thread0.cacheBlocked"
		"ipc_total"
		
	Metrics repeated for an unknown number of instances:
		"L1Dcache.demand_hits"
		"L1Dcache.demand_misses"
		"L1Dcache.demand_accesses"
		"L2cache.demand_hits"
		"L2cache.demand_misses"
		"L2cache.demand_accesses"
		
	
"""

#	Key terms for the simulation output file: ruby.stats
ruby_stats_keyterms = ["latency", "hit latency", "miss latency", "Total_delay_cycles"]
list_avg_latency = []
list_std_dev_latency = []

list_avg_hit_latency = []
list_std_dev_hit_latency = []

list_avg_miss_latency = []
list_std_dev_miss_latency = []

list_avg_Total_delay_cycles = []
list_std_dev_Total_delay_cycles = []

"""
	Additional notes:
	"Total_delay_cycles" is repeated 4 times.
	
	The other metrics are repeated for an unknown number of times.
	
	IMPORTANT NOTE:
		Distinguish "latency" from "hit latency" and "miss latency".
"""




#	Filename for output results file.
op_file = "output-file.txt"
#	Create file object for output results file.
op_f_obj = open(op_file, "w");














#	=======================================================


"""
	Method to determine the arithmetic mean of a list
		of numbers.
	@param list_num		A number to determine the arithmetic
						mean of this list of numbers.
	@return				Arithmetic mean for the list.
"""
def get_average(list_num):
	if	0 == len(list_num):
		return 0
	else:
		# Arithmetic mean = Sum / Number of elements in the list.
		return (sum(list_num)/len(list_num))





"""
	Method to determine the standard deviation of a list
		of numbers.
	
	\sigma = \sqrt{\sum^{N}_{i=1} (x_i - amean)^2}
	
	@param list_num		A number to determine the standard
						deviation of this list of numbers.
	@return				Standard deviation for the list.
"""
def get_std_dev(list_num):
	# Arithmetic mean = Sum / Number of elements in the list.
	amean = (sum(list_num)/len(list_num))
	# Geometric mean
	gmean = 0
	for i in list_num:
		gmean = gmean +  pow(list_num[i]-amean,2)
	return sqrt(gmean/len(list_num))




"""
	Method to process "ruby.stats".
	
	@param path		Path to the "ruby.stats" file.
	@return			Nothing.
"""
def process_ruby_stats_all(path):
	#op_f_obj.write(path+"\n")
	#	Create file object for "ruby.stats" file.
	ip_f_obj = open(path, "r")
	#	Number of sections beginning with "Profiler Stats"
	num_sections = 0
	#	String distinguishing different sections
	profiler_stats = "Profiler Stats"
	#	Current latency averages + standard deviation
	list_cur_avg_latency = []
	list_cur_std_dev_latency = []
	#	Current hit latency averages + standard deviation
	list_cur_avg_hit_latency = []
	list_cur_std_dev_hit_latency = []
	#	Current miss latency averages + standard deviation
	list_cur_avg_miss_latency = []
	list_cur_std_dev_miss_latency = []
	#	Current Total_delay_cycles + standard deviation
	list_avg_Total_delay_cycles = []
	list_std_dev_Total_delay_cycles = []
	#	Enumerate all lines of the input text file.
	for ln_num, cur_ln in enumerate(ip_f_obj):
		#	Does current line contain "Profiler Stats"?
		if profiler_stats in cur_ln:
			num_sections = num_sections + 1
		if 4 < num_sections:
			raise IOError("ruby.stats contains >4 sections")
		#	Tokenize the string
		temp_str = cur_ln
		tokens = str.split(temp_str)
		if 0 < len(tokens):
			if tokens[0] == "latency:":
				#print "Yes. Token #1 is latency"
				for ti, tk in enumerate(tokens):
					if tk == "average:":
						#print "avg:"+tokens[ti+1]+"="
						list_cur_avg_latency.append(float(tokens[ti+1]))
					if tk == "deviation:":
						#print "std dev:"+tokens[ti+1]+"="
						list_cur_std_dev_latency.append(float(tokens[ti+1]))
			if tokens[0] == "hit" and tokens[1] == "latency:":
				#print "Yes. Token #1 is latency"
				for ti, tk in enumerate(tokens):
					if tk == "average:":
						#print "avg:"+tokens[ti+1]+"="
						list_cur_avg_hit_latency.append(float(tokens[ti+1]))
					if tk == "deviation:":
						#print "std dev:"+tokens[ti+1]+"="
						list_cur_std_dev_hit_latency.append(float(tokens[ti+1]))
			if tokens[0] == "miss" and tokens[1] == "latency:":
				#print "Yes. Token #1 is latency"
				for ti, tk in enumerate(tokens):
					if tk == "average:":
						#print "avg:"+tokens[ti+1]+"="
						list_cur_avg_miss_latency.append(float(tokens[ti+1]))
					if tk == "deviation:":
						#print "std dev:"+tokens[ti+1]+"="
						list_cur_std_dev_miss_latency.append(float(tokens[ti+1]))
			if tokens[0] == "Total_delay_cycles:":
				#print "Yes. Token #1 is latency"
				for ti, tk in enumerate(tokens):
					if tk == "average:":
						#print "avg:"+tokens[ti+1]+"="
						list_avg_Total_delay_cycles.append(float(tokens[ti+1]))
					if tk == "deviation:":
						#print "std dev:"+tokens[ti+1]+"="
						list_std_dev_Total_delay_cycles.append(float(tokens[ti+1]))
	list_avg_latency.append(list_cur_avg_latency)
	op_f_obj.write("list of average latency:\n")
	for key in list_cur_avg_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_std_dev_latency.append(list_cur_std_dev_latency)
	op_f_obj.write("list of standard deviation latency:\n")
	for key in list_cur_avg_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_avg_hit_latency.append(list_cur_avg_hit_latency)
	op_f_obj.write("list of average hit latency:\n")
	for key in list_cur_avg_hit_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_std_dev_hit_latency.append(list_cur_std_dev_hit_latency)
	op_f_obj.write("list of standard deviation hit latency:\n")
	for key in list_cur_std_dev_hit_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_avg_miss_latency.append(list_cur_avg_miss_latency)
	op_f_obj.write("list of average miss latency:\n")
	for key in list_cur_avg_miss_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_std_dev_miss_latency.append(list_cur_std_dev_miss_latency)
	op_f_obj.write("list of standard deviation miss latency:\n")
	for key in list_cur_std_dev_miss_latency:
		op_f_obj.write(str(key) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_avg_Total_delay_cycles.append(list_avg_Total_delay_cycles)
	op_f_obj.write("list of average Total_delay_cycles:\n")
	for key in xrange(4):
		op_f_obj.write(str(list_avg_Total_delay_cycles[key]) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_std_dev_Total_delay_cycles.append(list_std_dev_Total_delay_cycles)
	op_f_obj.write("list of standard deviation Total_delay_cycles:\n")
	for key in xrange(4):
		op_f_obj.write(str(list_std_dev_Total_delay_cycles[key]) + " ")
		#op_f_obj.write(" ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	#	Close the input file object.
	ip_f_obj.close()
	
	






"""
	Method to process "stats.txt".
	
	@param path		Path to the "stats.txt" file.
	@return			Nothing.
"""
def process_stats_txt(path):
	op_f_obj.write(path+"\n")
	#	Create file object for "stats.txt" file.
	ip_f_obj = open(path, "r")
	#	Number of sections beginning with "Begin simulation statistics"
	num_sections_begin = 0
	#	Number of sections beginning with "End simulation statistics"
	num_sections_end = 0
	#	List of current values for important metrics.
	list_cur_sim_seconds = []
	list_cur_host_seconds = []
	list_cur_percent_links_utilized = []
	list_cur_rename_LSQFullEvents = []
	list_cur_lsq_thread0_cacheBlocked = []
	list_cur_ipc_total = []
	list_cur_L1Dcache_demand_hits = []
	list_cur_L1Dcache_demand_misses = []
	list_cur_L1Dcache_demand_accesses = []
	list_cur_L2cache_demand_hits = []
	list_cur_L2cache_demand_misses = []
	list_cur_L2cache_demand_accesses = []
	#	String distinguishing different sections
	Begin_simulation_statistics = "Begin Simulation Statistics"
	End_simulation_statistics = "End Simulation Statistics"
	#	Enumerate all lines of the input text file.
	for ln_num, cur_ln in enumerate(ip_f_obj):
		#	Does current line contain "Begin Simulation Statistics"?
		if Begin_simulation_statistics in cur_ln:
			num_sections_begin = num_sections_begin + 1
		#	Does current line contain "End Simulation Statistics"?
		if End_simulation_statistics in cur_ln:
			num_sections_end = num_sections_end + 1
#		print "num_sections_begin:"+str(num_sections_begin)+"="
#		print "num_sections_end:"+str(num_sections_end)+"="
		"""
			BUG FOUND!!!
			
			Cannot add results to the lists for stst.txt
			Constraints on determining the third section
				may be too tight.
			Slacken the constraints.
		"""
		if 4 < num_sections_begin or 4 < num_sections_end:
			raise IOError("ruby.stats contains >4 sections")
		#if 3 == num_sections_begin and 2 == num_sections_end:
		if 3 == num_sections_begin:
			#	Tokenize the string
			temp_str = cur_ln
			tokens = str.split(temp_str)
			#	If the line is not empty...
			if 0 < len(tokens):
				print "<<			Non-empty token:"+tokens[0]+"=="
				print "???		Line number:"+str(ln_num)+"=="
				if "sim_seconds" in tokens[0]:
					list_cur_sim_seconds.append(float(tokens[1]))
				if "host_seconds" in tokens[0]:
					list_cur_host_seconds.append(float(tokens[1]))
				if "percent_links_utilized" in tokens[0]:
					print "<<			Current token:"+tokens[0]+"=="
					"""
						IMPORTANT NOTE: Design decision
						Append results for all routers.
						Only use results from section 3.
					"""
					list_cur_percent_links_utilized.append(float(tokens[1]))
				if "rename.LSQFullEvents" in tokens[0]:
					list_cur_rename_LSQFullEvents.append(float(tokens[1]))
				if "lsq.thread0.cacheBlocked" in tokens[0]:
					list_cur_lsq_thread0_cacheBlocked.append(float(tokens[1]))
				if "ipc_total" in tokens[0]:
					list_cur_ipc_total.append(float(tokens[1]))
				if "L1Dcache.demand_hits" in tokens[0]:
					list_cur_L1Dcache_demand_hits.append(float(tokens[1]))
				if "L1Dcache.demand_misses" in tokens[0]:
					list_cur_L1Dcache_demand_misses.append(float(tokens[1]))
				if "L1Dcache.demand_accesses" in tokens[0]:
					list_cur_L1Dcache_demand_accesses.append(float(tokens[1]))
				if "L2cache.demand_hits" in tokens[0]:
					list_cur_L2cache_demand_hits.append(float(tokens[1]))
				if "L2cache.demand_accesses" in tokens[0]:
					list_cur_L2cache_demand_accesses.append(float(tokens[1]))
				if "L2cache.demand_misses" in tokens[0]:
					list_cur_L2cache_demand_misses.append(float(tokens[1]))
	list_sim_seconds.append(list_cur_sim_seconds)
	op_f_obj.write("list of sim_seconds:\n")
	for key in list_cur_sim_seconds:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_host_seconds.append(list_cur_host_seconds)
	op_f_obj.write("list of host_seconds:\n")
	for key in list_cur_host_seconds:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_percent_links_utilized.append(list_cur_percent_links_utilized)
	op_f_obj.write("list of percent_links_utilized:\n")
	for key in list_cur_percent_links_utilized:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_rename_LSQFullEvents.append(list_cur_rename_LSQFullEvents)
	op_f_obj.write("list of rename.LSQFullEvents:\n")
	for key in list_cur_rename_LSQFullEvents:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_lsq_thread0_cacheBlocked.append(list_cur_lsq_thread0_cacheBlocked)
	op_f_obj.write("list of lsq.thread0.cacheBlocked:\n")
	for key in list_cur_lsq_thread0_cacheBlocked:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_ipc_total.append(list_cur_ipc_total)
	op_f_obj.write("list of ipc_total:\n")
	for key in list_cur_ipc_total:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L1Dcache_demand_hits.append(list_cur_L1Dcache_demand_hits)
	op_f_obj.write("list of L1Dcache.demand_hits:\n")
	for key in list_cur_L1Dcache_demand_hits:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L1Dcache_demand_misses.append(list_cur_L1Dcache_demand_misses)
	op_f_obj.write("list of L1Dcache.demand_misses:\n")
	for key in list_cur_L1Dcache_demand_misses:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L1Dcache_demand_accesses.append(list_cur_L1Dcache_demand_accesses)
	op_f_obj.write("list of L1Dcache.demand_accesses:\n")
	for key in list_cur_L1Dcache_demand_accesses:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L2cache_demand_hits.append(list_cur_L2cache_demand_hits)
	op_f_obj.write("list of L2cache.demand_hits:\n")
	for key in list_cur_L2cache_demand_hits:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L2cache_demand_misses.append(list_cur_L2cache_demand_misses)
	op_f_obj.write("list of L2cache.demand_misses:\n")
	for key in list_cur_L2cache_demand_misses:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	list_L2cache_demand_accesses.append(list_cur_L2cache_demand_accesses)
	op_f_obj.write("list of L2cache.demand_accesses:\n")
	for key in list_cur_L2cache_demand_accesses:
		op_f_obj.write(str(key) + " ")
	op_f_obj.write("\n")
	op_f_obj.write("\n")
	#	Close the input file object.
	ip_f_obj.close()





"""
	Functions not implemented due to a lack of time;
		get_geometric_mean()
		
"""


#	=======================================================

print "========================================================="

print "Preliminaries: Set up file objects/streams for processing."

#	Check if the given directory is valid
if os.path.isdir(location_results):
	print "== Given location of the benchmark is a valid directory."
#	elif BLAH:
else:
	raise IOError("Benchmark location is INVALID. Arrivederci!")



#	============================================================
#	Processing important metrics in simulation results.

"""
	Author's note:
	Avoid changing to benchmark directories, so that I can improve
		computational efficiency.
	
	To access a given file containing simulation results
		(e.g., "simres") for a given particular benchmark bmk-i,
		and for the given location of benchmark directories
		[location], try the following to access the file:
		
		[location]/[bmk-i]/[simres]
"""

print "---------------------------------------------------------"
print "Processing gem5 simulation results."


#	For each input benchmark
for index, bmk in enumerate(bmk_names):
	#	Check if each given benchmark directory is valid.
	if os.path.isdir(location_results+"/"+bmk):
		print "== bmk location:"+location_results+"/"+bmk+"#"
		#	Filename to be processed.
		fname = location_results+"/"+bmk+"/"+sim_op_files[st_in_sof]
		#	Check if simulation results file is valid
		if os.path.isfile(fname):
			print ">		Sim o/p:"+fname+"#"
			#	Process "stats.txt"
			process_stats_txt(fname)
		else:
			raise IOError("stats.txt:"+fname)
		fname = location_results+"/"+bmk+"/"+sim_op_files[rs_in_sof]
		if os.path.isfile(fname):
			print ">		Sim o/p:"+fname+"#"
			#	Process "ruby.stats"
			process_ruby_stats_all(fname)
		else:
			raise IOError("ruby.stats:"+fname)
	else:
		raise IOError("Benchmark location is INVALID:"+location_results+"/"+bmk+"/"+sim_op_files[rs_in_sof])





























#	Close the output file object.
op_f_obj.close()
print "==	End of statistical analysis."
print "==============================================="


