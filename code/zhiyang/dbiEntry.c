/*
########################################################################   
# Program Name: dbiIndex.c										       #
# Author: Anthony C. Rivera Carvelli                                   #
# Date: 12/3/2014                                                      #
#                                                                      #
# Info: This is a proof of concept to get the dirty block index working#
# on SimpleScalar. Compile this program on the simple scalar main      #
# directory to get it working because it uses a library from           #
# SimpleScalar. The tag store structure is not the same one used on    #
# SimpleScalar and was provided just as a proof of concept. I will     #
# implement it with the original one later on                          #
########################################################################

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Modified by Zhiyang Ong
 * Include Standard C Library
 */
#include <stdlib.h>
#include <stdio.h>
#include "dbiEntry.h"


/* unsigned int DBI_GRANULARITY = 4; */



/**
 * Modified by Zhiyang Ong. Changed/Added comments.
 *
 * Function to print the contents of the DBI entry.
 * @param dbie		An entry for the ditry-block index.
 * @return			Nothing
 */
void dbiToString(struct dirtyBlockIndexEntry dbie) {
	printf("	dbie.validStatus:%i.\n", dbie.validStatus);
	printf("	dbie.rowTag:%i.\n", dbie.rowTag);
	// Enumerate the dirty bit vector
	int i;
	for(i=0; i<DBI_GRANULARITY; i++) {
		printf("	dbie.validStatus[%i]:%i.\n", i, dbie.dirtyBitVector[i]);
	}
}















