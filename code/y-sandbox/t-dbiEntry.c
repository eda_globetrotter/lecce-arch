/**
 * This is written by Zhiyang Ong to test the struct dbiEntry.
 *
 * This is a refactoring of the code that Anthony implemented to
 *	enforce the clean separation between header file declaration,
 *	C-file implementation, and test suite implementation.
 * 
 * I want the header file declaration and the C-file implementation
 *	ready to be ported to the real code base.
 *
 * Hence, the test suite for the code shall be separate from these.
 *
 *
 *	Synopsis:
 *		Create a DBI.
 *		Allocate memory to the DBI.
 *		Create DBI entries in the DBI.
 *		Assign values to the properties of these DBI entries.
 *		Use the toString() function for each DBI entry to print its contents.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * To ensure that the results of the automated regression testing
 *	are repeatable and replicable, set the parameters in cache.h
 *	according to the following.
 * #define CACHE_BLK_VALID		0x00000001
 * #define CACHE_BLK_DIRTY		0x00000002
 * #define DBI_SIZE								6
 * #define DBI_GRANULARITY						9
 * #define LEAST_RECENTLY_WRITTEN				0
 * #define LRW_W_BIMODAL_INSERTION				1
 * #define REWRITE_INTERVAL_PREDICTION			2
 * #define MAX_ENTRY							3
 * #define MIN_ENTRY							4
 * #define DBI_CACHE_REPLACEMENT_POLICY			LEAST_RECENTLY_WRITTEN
 *
 *
 *
 *
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */

/**
 * Modified by Zhiyang Ong
 * Include Standard C Library
 */
#include <stdlib.h>
#include <stdio.h>


/* Include the Header file for our code. */
#include "dbiEntry.h"

int main()
{
printf("++=		Testing the DBI.\n");
	/* =============================================== */

	/* Test if DBI and DBI entry implementation works */
	int c_size = 6;
	int bv_size = 9;
	int i,j;

printf("++=		Create the DBI.\n");
	/* Create a DBI of size 5. */
	struct dirtyBlockIndexEntry *c;
printf("++=		About to allocate memory for DBI.\n");

/* ===================================================== */
	/**
	 * Allocate memory space to the DBI.
	 *
	 * When allocating memory to the DBI, the user must also
	 *	allocate memory to the dirty bit vector for each DBI
	 *	entry in the DBI.
	 */
	c = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*DBI_SIZE);
printf("++=		Allocated memory for DBI.\n");
	/* Allocate memory space to the dirty-bit vector of each DBI entry*/
	for(i=0; i<DBI_SIZE; i++) {
		c[i].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*DBI_GRANULARITY);
	}
/**
 * Attempted to allocate memory in a separate function. It failed.
 *
	for(i=0; i<DBI_SIZE; i++) {
		dbiEntryAllocateMemory(c[i]);
	}
*/
/* ===================================================== */
	/* For each DBI entry in the DBI. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Assign values to the DBI entry. */
		c[i].validStatus = i*10+1;
		c[i].rowTag = i + 200;
		// Populate the dirty-bit vector.
		for(j=0; j<DBI_GRANULARITY; j++) {
			c[i].dirtyBitVector[j] = j%2;
		}
		c[i].nthWriteOperation = i*i+500;
	}
	
	// Set the dirty-bit vector of dbi[3] to: 101101110.
	c[3].dirtyBitVector[0] = false;
	c[3].dirtyBitVector[1] = true;
	c[3].dirtyBitVector[2] = true;
	c[3].dirtyBitVector[3] = true;
	c[3].dirtyBitVector[4] = false;
	c[3].dirtyBitVector[5] = true;
	c[3].dirtyBitVector[6] = true;
	c[3].dirtyBitVector[7] = false;
	c[3].dirtyBitVector[8] = true;
	
	/* For each DBI entry in the DBI. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Print contents of currently enumerated DBI entry */
		dbiEntryToString(c[i]);
		printf("\n");
	}

	if(3 == DBI_CACHE_REPLACEMENT_POLICY) {
		printf("Chosen Max Entry replacement policy\n");
	}

	printf("Number of dirty bits of c[3]'s DBV are:%i.\n", getNumberOfDirtyBitsInDBV(c[3]));

	return 0;
}
