\beamer@endinputifotherversion {3.26pt}
\beamer@sectionintoc {2}{Acknowledgments}{3}{0}{1}
\beamer@sectionintoc {3}{Dirty-Block Index (DBI) Structure}{5}{0}{2}
\beamer@sectionintoc {4}{Typesetting Tables in {\rm \LaTeX }\ Documents}{15}{0}{3}
\beamer@sectionintoc {5}{Including Graphics in {\rm \LaTeX }\ Documents}{18}{0}{4}
\beamer@sectionintoc {6}{References}{21}{0}{5}
