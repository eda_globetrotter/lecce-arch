/*
########################################################################   
# Program Name: dbiIndex.h										       #
# Author: Anthony C. Rivera Carvelli                                   #
# Date: 12/3/2014                                                      #
#                                                                      #
# Updates:                                                             #
########################################################################

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//NOTE: Library machine.h is called from SimpleScalar just for the sake   
//      of using some value types that SimpleScalar uses just to make 
//      the implementation on SimpleScalar easier. That code belongs to 
//      Todd M. Austin, Ph.D. and SimpleScalar, LLC. 

#include <stdio.h>
#include "machine.h"

//Block status values
/* block in valid, in use */
#define CACHE_BLK_VALID		0x00000001
/* dirty block */	
#define CACHE_BLK_DIRTY		0x00000002	


struct dirtyBitIndexEntry
{
	unsigned int validStatus;
	//Log2 # of rows on DRAM
	md_addr_t rowTag; 
	//Pointer
	unsigned int *dirtyBitVector; 	
};


