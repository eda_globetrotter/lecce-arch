/*
########################################################################   
# Program Name: dbiIndex.h										   #
# Author: Anthony C. Rivera Carvelli                                   #
# Date: 12/3/2014                                                      #
#                                                                      #
# Updates:                                                             #
########################################################################

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Include Headers from the C Standard Library */
#include <stdio.h>

//NOTE: Library machine.h is called from SimpleScalar just for the sake   
//      of using some value types that SimpleScalar uses just to make 
//      the implementation on SimpleScalar easier. That code belongs to 
//      Todd M. Austin, Ph.D. and SimpleScalar, LLC. 
/**
 * machine.h includes the definition of the struct md_addr_t,
 *	which is of the type "qword_t".
 * host.h defines the type "qword_t" as an "unsigned long long".
 */
#include "machine.h"

//Block status values
/* block in valid, in use */
#define CACHE_BLK_VALID		0x00000001
/* dirty block */	
#define CACHE_BLK_DIRTY		0x00000002	
/**
 * Modified by Zhiyang Ong.
 * Constants for configuring the DBI and DBI entries.
 *
 * Tried to implement this with "const" and/or "extern" in vain.
 */
/* DBI Size */
#define DBI_SIZE						6
/* DBI granularity. */
#define DBI_GRANULARITY					9
/**
 * List of DBI replacement policies
 * Least Recently Written (LRW)
 */
#define LEAST_RECENTLY_WRITTEN			0
/* LRW with bimodal insertion */
#define LRW_W_BIMODAL_INSERTION			1
/* Rewrite-interval prediction policy */
#define REWRITE_INTERVAL_PREDICTION		2
/* Max Dirty - Entry with maximum number of dirty blocks. */
#define MAX_ENTRY						3
/* Min Dirty - Entry with minimum number of dirty blocks. */
#define MIN_ENTRY						4
/* Selected DBI replacement policy. */
#define DBI_CACHE_REPLACEMENT_POLICY	LEAST_RECENTLY_WRITTEN






/**
 * Modified by Zhiyang Ong. Changed/Added comments.
 *
 * Struct to define the properties of each DBI entry.
 */
struct dirtyBlockIndexEntry
{
	/* Valid bit for DBI entry */
	unsigned int validStatus;
	/* Row number in the cache; Log2 # of rows on DRAM */
	md_addr_t rowTag;
	/* Represents dirty bits of cache blocks in the given cache row. */
	unsigned int *dirtyBitVector; 	
};

/**
 * Modified by Zhiyang Ong. Changed/Added comments.
 *
 * Function to print the contents of the DBI entry.
 * @param dbie		An entry for the ditry-block index.
 * @return			Nothing
 */
void dbiToString(struct dirtyBlockIndexEntry dbie);

