\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Motivation}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Background Information}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Related Work}{3}{section.2}
\contentsline {section}{\numberline {3}Dirty-Block Index Structure}{3}{section.3}
\contentsline {section}{\numberline {4}Dirty-Block Index Operations}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Dirty-Block Index Replacement Policies}{4}{subsection.4.1}
\contentsline {section}{\numberline {5}Performance Analysis Methodology}{5}{section.5}
\contentsline {section}{\numberline {6}Experimental Results and Discussion}{5}{section.6}
\contentsline {section}{\numberline {7}Future Work}{6}{section.7}
\contentsline {section}{\numberline {8}Conclusions}{7}{section.8}
