/**
 * This is written by Zhiyang Ong to define the struct for the
 *	Dirty-Block Index (DBI).
 *
 * This defines the Dirty-Block Index (DBI), in terms of its properties
 *	and its behavior/functions.
 * 
 *
 *
 *	Synopsis:
 *		Declare a dynamic array of DBI entries; this declares the DBI.
 *		Declare DBI replacement policies and DBI operations.
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */

/* Include Standard C Library */
#include <limits.h>
/* Include Headers from our modified source code of SimpleScalar. */
#include "dbi.h"



/* ===================================================== */
/* Support functions for the DBI. */

/**
 * Function to print the contents of the DBI.
 * @param		None.
 * @return		Nothing.
 */
void dbiToString(struct dirtyBlockIndex dbiObj) {
	int i,j;
	for(i=0; i<DBI_SIZE; i++) {
		/* Print contents of currently enumerated DBI entry */
		dbiEntryToString(dbiObj.dbi[i]);
		printf("\n");
	}
}

/**
 * Function to allocate memory for the DBI.
 * @param		None.
 * @return		Nothing.
 *
 * Modified by Anthony C. Rivera
 * Created dbiAllocateMemory function by receiving the value dbie
 * as a pointer (which is the same as being sent by reference) 
 * I use -> instead of a period because this is how you reference the
 * members of a structure that is a pointer. Code below is a slight 
 * modification of the code I created in dbiIndex.c.
 *
 * Modified by Zhiyang Ong
 * Change memory space allocation from "unsigned int" to "bool".
 */
void dbiAllocateMemory(struct dirtyBlockIndex *dbie) 
{
	int count;
	// allocate space for dirtyBlockIndexEntry array called dbi
	dbie->dbi = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*DBI_SIZE);    
	 
	for (count = 0; count < DBI_SIZE; count++)
	{
		//allocate space for dirtyBitVector (array) on each struct on the array
		dbie->dbi[count].dirtyBitVector = (bool *)malloc(sizeof(bool)*DBI_GRANULARITY);
	}
	
}






/* ===================================================== */
/* DBI replacement policies. */

/**
 * Function to DBI replacement via the Least Recently
 *	Written (LRW) policy.
 *
 * Sort the DBI entries based on the time that they are last
 *	written to.
 *
 * @param dbie		A DBI entry to replace the LRW entry in DBI.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiLeastRecentlyWritten(struct dirtyBlockIndexEntry dbie) {
	/* For the given ... */
	
	/* FIX THIS!!! */
	
	return dbie.rowTag;
}


/**
 * Function to DBI replacement via max dirty policy.
 * @param dbiObj	A DBI used to determine the DBI entry with
 *						maximum number of bits.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via max dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMaxDirtyPolicy(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie) {
	/* Index of DBI entry with the maximum number of dirty bits. */
	unsigned int indexDBIEntryMaxNumOfDirtyBits = UINT_MAX;
	/* Current maximum number of dirty bits in enumerated DBI entries. */
	unsigned int currentMaxNumOfDirtyBits = 0;
	/* Number of dirty bits of currently enumerated DBI entry. */
	int numOfDirtyBitsInDbiEntry = 0;
	
	/* Index for enumerating dynamic arrays. */
	int i = 0;
	
	/* Enumerate all the DBI entries and get the maximum. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Get number of dirty bits for DBI entry. */
		numOfDirtyBitsInDbiEntry = getNumberOfDirtyBitsInDBV(dbiObj.dbi[i]);
		/* Is number of dirty bits > current maximum? */
		if(numOfDirtyBitsInDbiEntry > currentMaxNumOfDirtyBits) {
			/**
			 * Update current maximum and index of the DBI entry with
			 *	maximum number of dirty bits with the index of the
			 *	corresponding DBI entry.
			 */
			currentMaxNumOfDirtyBits = numOfDirtyBitsInDbiEntry;
			indexDBIEntryMaxNumOfDirtyBits = i;
		}
	}
	
	/**
	 * Store the row tag of the DBI entry to be evicted, before
	 *	replacing the DBI entry with the maximum number of dirty bits.
	 */
	md_addr_t evictedRowTag = dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].rowTag;
	
	/**
	 * Replace the contents of the DBI entry at the index with the
	 *	maximum number of dirty bits with the contents of "dbie".
	 */
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].validStatus = dbie.validStatus;
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].rowTag = dbie.rowTag;
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].nthWriteOperation = dbie.nthWriteOperation;
	for(i=0; i<DBI_GRANULARITY; i++) {
		dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].dirtyBitVector[i] = dbie.dirtyBitVector[i];
	}
	
	
	return evictedRowTag;
}


/**
 * Function to DBI replacement via min dirty policy.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via min dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMinDirtyPolicy(struct dirtyBlockIndexEntry dbie) {
	return dbie.rowTag;
}

//Modified by Anthony C. Rivera
/**
 * Function to DBI replacement via min dirty policy.
 * @param dbi		DBI structure that is going to be updated.
 * @param rTag      Row tag that we are searching for.
 * @param dbvIndex  Index of dirty bit that we need to mark as
 *                  true. 
 * @return			Nothing.
 */
 /* Created this function, it takes the row tag and searches for the 
  * row on the dbi. If it's found it uses the variable dbvIndex as an
  * index of which dirty bit vector variable needs to be marked as true.
  * If it is not found then we call the replacement policy that the user
  * picked.
  */
void modifyMetadataDBI(md_addr_t rTag, unsigned int dbvIndex, struct dirtyBlockIndex dbi)
{
	bool found = false;
	int count;
	struct dirtyBlockIndexEntry dbie;
	dbie.dirtyBitVector = (bool *)malloc(sizeof(bool)*DBI_GRANULARITY);
	
	for(count = 0; count < DBI_SIZE; count++)
	{
		if(dbi.dbi[count].rowTag == rTag)
		{
			dbi.dbi[count].dirtyBitVector[dbvIndex] = true;
			found = true;
			break;
		}
	}
	
	if(found == false)
	{
		switch(DBI_CACHE_REPLACEMENT_POLICY)
		{
			case LEAST_RECENTLY_WRITTEN:
			break;
			case LRW_W_BIMODAL_INSERTION:
			break;
			case REWRITE_INTERVAL_PREDICTION:
			break;
			case MAX_ENTRY:
			dbiMaxDirtyPolicy(dbi, dbie);
			break;
			case MIN_ENTRY:
			dbiMinDirtyPolicy(dbie);
			break;
		}
	}	
}







