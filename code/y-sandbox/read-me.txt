Testing the code requires a subset of the header files of SimpleScalar.

Hence, I have copied all the header files to this directory.











IMPORTANT NOTES:
#	Design choice:
		Do not implement the feature for configuring the parameters via the option/flag in the command line.
		This helps us to save some development time implementing and testing the code to support this feature.
		Use the C macros in the file "dbiIndex.h" to set the parameters for organization.
		We have placed all the parameters for configuration in the "dbiEntry.h" file makes it easy for the user to configure the DBI for another DBI configuration in one file/location.

#	The "C" implementation file shall include only the implementation of functions defined in the header file. The main() function for test suites shall be stored in another file, to practise separation of concerns.

#	Implement the DBI as a dynamic array. Declare it and its operations in "dbi.h", and implement these in "dbi.c".
	This keeps the implementation of the DBI separate from the cache.

#	Enforce separate implementation of DBI and DBI entry to support regression testing.

#	To implement the DBI replacement policies, I need to track the following:
		- Number of dirty bits in the dirty bit vector.
			For dbiMinDirtyPolicy()
			For dbiMaxDirtyPolicy()
		- The number of writes to the DBI, since the processor has
			started running.
			For dbiLeastRecentlyWritten()
			For dbiLRWBimodalInsertion()
	Consequently, I need to include two more fields in the DBI entry.
	I intend to represent them as integers, even though their implementation
		on silicon would be bit vectors.
	I have chosen integers over bit vectors to make sorting the DBI entries
		in the DBI easier.
	Hence, I need to extend the DBI entry struct to include the number of
		writes to the DBI as a "global" variable that can be accessed and
		updated by different functions.
	Also, I need to track the $n^{th}$ write operation to a DBI entry
		as a field/property.
		This does not require an accessor function.
		As for the number of dirty bits, do not include it as a
			field/property.
			This would require the field to be updated whenever an index
				in the dirty bit vector changes.
			However, I can still provide access to the number of dirty
				bits in the dirty bit vector via an accessor function.
			




Default cache configuration:
2 level cache system
Number of banks = ???
set in SimpleScalar = index of set associative cache = row # of cache



1) L2D
2) L2D + L2I
3) L2I
4) L2D + L1D
If 4) is no faster than 1, 
  Using DBI for L1 does not bring about performance speedup because of greater latency due to the DBI.

Shen2005, data cache size = instruction cache size = 64KB
L1D, L1I, 64 KB

DL1, 1028 rows, 32 block size, 4 way set
DL2, 1024 rows, 64, 4 way
Il1, 512, 32, 1
Il2, 1024 rows, 64, 4 way
unified cache



http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=584667&tag=1
board level cache from 1-16 MB

64KB,2-way set associative I cache
8KB direct-mapped I cache


set in SimpleScalar = row # in cache = index of set associative cache
granularity = associativity of cache

Question: Implementation of shared L2 and/or L3 is beyond the scope of this project.
opt_reg_note
line 781, sim-outorder.c



























