/*
########################################################################   
# Program Name: dbiIndex.c										       #
# Author: Anthony C. Rivera Carvelli                                   #
# Date: 12/3/2014                                                      #
#                                                                      #
# Info: This is a proof of concept to get the dirty block index working#
# on SimpleScalar. Compile this program on the simple scalar main      #
# directory to get it working because it uses a library from           #
# SimpleScalar. The tag store structure is not the same one used on    #
# SimpleScalar and was provided just as a proof of concept. I will     #
# implement it with the original one later on                          #
########################################################################

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include "dbiIndex.h"

int main()
{
	struct dirtyBitIndexEntry *dbi;
	
	int dbiSize = 50;
	int dbiGranularity = 50;
	int count;
	
	// allocate space for dirtyBitIndexEntry array called dbi
	dbi = (struct dirtyBitIndexEntry *)malloc(sizeof(struct dirtyBitIndexEntry)*dbiSize);      
	 
	for (count = 0; count < dbiSize; count++)
	{
		//allocate space for dirtyBitVector (array) on each struct on the array
		dbi[count].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*dbiGranularity);  
	} 

}
