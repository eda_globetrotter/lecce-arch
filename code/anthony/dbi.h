/**
 * This is written by Zhiyang Ong to define the struct for the
 *	Dirty-Block Index (DBI).
 *
 * This defines the Dirty-Block Index (DBI), in terms of its properties
 *	and its behavior/functions.
 * 
 *
 *
 *	Synopsis:
 *		Declare a dynamic array of DBI entries; this declares the DBI.
 *		Declare DBI replacement policies and DBI operations.
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */

/* Include Headers from our modified source code of SimpleScalar. */
/**
 * machine.h includes the definition of the struct md_addr_t,
 *	which is of the type "qword_t".
 * host.h defines the type "qword_t" as an "unsigned long long".
 */
#include "machine.h"
/* Allow DBI entries to be created and inserted into the DBI. */
#include "dbiEntry.h"



/**
 * Struct to define a dynamic array of DBI entries.
 */
struct dirtyBlockIndex
{
	/* Dynamic array of DBI entries */
	struct dirtyBlockIndexEntry *dbi;
};

/* ===================================================== */
/* Support functions for the DBI. */

/**
 * Function to allocate memory for the DBI.
 * @param		None.
 * @return		Nothing.
 *
 * Modified by Anthony Rivera
 * Sent dbie as a pointer which is the same as sending it
 * by reference. Function now works.	
 */
void dbiAllocateMemory(struct dirtyBlockIndex *dbie);

/**
 * Function to print the contents of the DBI.
 * @param		None.
 * @return		Nothing.
 */
void dbiToString(struct dirtyBlockIndex dbiObj);



/* ===================================================== */
/* DBI replacement policies. */

/**
 * Function to DBI replacement via the Least Recently
 *	Written (LRW) policy.
 * @param dbie		A DBI entry to replace the LRW entry in DBI.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiLeastRecentlyWritten(struct dirtyBlockIndexEntry dbie);


/**
 * Function to DBI replacement via the Least Recently
 *	Written (LRW) policy with bimodal insertion.
 * @param dbie		A DBI entry to replace the LRW entry in DBI,
 *						via LRW with bimodal insertion policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiLRWBimodalInsertion(struct dirtyBlockIndexEntry dbie);


/**
 * Function to DBI replacement via the rewrite-interval prediction
 *	policy.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via rewrite-interval prediction policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiRewriteIntervalPrediction(struct dirtyBlockIndexEntry dbie);


/**
 * Function to DBI replacement via max dirty policy.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via max dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMaxDirtyPolicy(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie);


/**
 * Function to DBI replacement via min dirty policy.
 * @param dbiObj	A DBI used to determine the DBI entry with
 *						maximum number of bits.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via min dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMinDirtyPolicy(struct dirtyBlockIndexEntry dbie);



/* ===================================================== */
/* DBI operations. */

/**
 * DBI operation: Read access to the cache.
 * Implemented in the "cache.[h|c]".
 *
 * DBI operation: Writeback request to the cache.
 * i/p: row and columm of cache, which is used to generate
 *	the following metadata: row tag, index to dirty bit vector
 * o/p: nothing
 * For subfunctions that make up writeback request:
 *	insert/update block in cache: no output
 *	insert/update metadata in DBI: no output
 *
 * DBI operation: Cache eviction.
 * i/p: dirty bit vector
 * o/p: nothing
 *
 * DBI operation: DBI eviction
 * i/p: row tag, because we are evicting rows, not blocks
 * o/p: none
 *
 * 
 *
 * read_access and cache_eviction are implemented in the cache.
 * writeback_request:
 *	insert/update block in cache - Implemented in cache
 *	insert/update metadata in DBI - Implemented in DBI
 *
 * DBI eviction - implemented in DBI
 */


/**
 * Function to insert/update metadata in DBI.
 * @param rTag			Row tag
 * @param dbvIndex		Index to the dirty bit vector
 * @param dbi           DBI structure 
 * @return				Nothing
 */
void modifyMetadataDBI(md_addr_t rTag, unsigned int dbvIndex, struct dirtyBlockIndex dbi);

/**
 * Function to evict DBI entry.
 * @param rTag			Row tag
 * @return				Nothing
 */
void dbiEviction(md_addr_t rTag);





