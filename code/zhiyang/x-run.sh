#!/bin/bash

#	This is written by Zhiyang Ong to run SimpleScalar,
#		with implementations for explicitly addressed miss status
#		holding registers (MSHRs) and inverted MSHR organization.
# 
#	Synopsis:
#	Run my modification of SimpleScalar for MSHR implementations.
#	
#	
#	
#	Revision History:
#	1)	November 13, 2014. Initial working version.
# 
# 
# 
# 	The MIT License (MIT)
# 
# 	Copyright (c) <2014> <Zhiyang Ong>
#
# 	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# 	Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"


#	doxygen doxygen-config


#	========================================================

echo "==	Simulating implementations of MSHRs."
echo "==	Run implementation for explicitly addressed MSHRs."
echo "	=blank="
echo "==> Run experiments for spec2000args/vpr"
cd ../spec2000args/vpr
#nohup time 
#./RUNvpr ../../simplesim-3.0/sim-outorder ../../spec2000binaries/vpr00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_original-vpr -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2
./RUNvpr ../../mod-simple-scalar/sim-outorder ../../spec2000binaries/vpr00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_explicitly-addr-mshr-vpr -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2



echo "==> Run experiments for spec2000args/twolf"
cd ../twolf
#nohup time 
#./RUNtwolf ../../simplesim-3.0/sim-outorder ../../spec2000binaries/twolf00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_original-twolf -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2
./RUNtwolf ../../mod-simple-scalar/sim-outorder ../../spec2000binaries/twolf00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_explicitly-addr-mshr-twolf -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2



echo "==> Run experiments for spec2000args/mgrid"
cd ../mgrid
#nohup time 
#./RUNmgrid ../../simplesim-3.0/sim-outorder ../../spec2000binaries/mgrid00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_original-mgrid -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2
./RUNmgrid ../../mod-simple-scalar/sim-outorder ../../spec2000binaries/mgrid00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_explicitly-addr-mshr-mgrid -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2


echo "==> Run experiments for spec2000args/ammp"
cd ../ammp
#nohup time 
#./RUNammp ../../simplesim-3.0/sim-outorder ../../spec2000binaries/ammp00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_original-ammp -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2
./RUNammp ../../mod-simple-scalar/sim-outorder ../../spec2000binaries/ammp00.peak.ev6 -max:inst 50000000 -fastfwd 20000000 -redir:sim sim_op_explicitly-addr-mshr-ammp -bpred 2lev -bpred:2lev 1 256 4 0 -bpred:ras 8 -bpred:btb 64 2


#	Return to original working directory.


#	========================================================
echo "==	Run implementation for inverted MSHR organization."






















