/**
 * This is written by Zhiyang Ong to test the concepts in C.
 *
 * This tests concepts in C.
 *
 *
 *	Synopsis:
 *		Test concepts in C.
 *		
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */



/* Include Standard C Library */
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

int main()
{
	/* =============================================== */

	int i,j;
	for(i=0; i<15; i++) {
		printf("Value of i is:%i.\n", i);
		if(i==7) {
			printf("	Terminate now!!!\n");
			break;
		}
	}
	printf("End loop.\n");
	printf("===\n");
	bool flag = false;
	if(!flag) {
		printf("	(Flag = false) => (!flag = true)\n");
	}
	//j = 5;
	j = 61;
	switch(j) {
		case 3:
			printf("3 is found\n");
			break;
		case 27:
			printf("27 is found\n");
			break;
		case 61:
			printf("61 is found\n");
			break;
		case (14):
			printf("14 is found\n");
			break;
		default:
			printf("92 is found\n");	
	}
	return 0;
}






















