/**
 * This is written by Zhiyang Ong to define the struct for the
 *	Dirty-Block Index (DBI).
 *
 * This defines the Dirty-Block Index (DBI), in terms of its properties
 *	and its behavior/functions.
 * 
 *
 *
 *	Synopsis:
 *		Declare a dynamic array of DBI entries; this declares the DBI.
 *		Declare DBI replacement policies and DBI operations.
 *
 *
 * The MIT License (MIT)
 *
 * Copyright (c) <2014> <Zhiyang Ong>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"		Don't compromise my computing accounts. You have been warned.
 */

/* Include Standard C Library */
#include <limits.h>
/* Include Headers from our modified source code of SimpleScalar. */
#include "dbi.h"



/* ===================================================== */
/* Support functions for the DBI. */

/**
 * Function to print the contents of the DBI.
 * @param		None.
 * @return		Nothing.
 */
void dbiToString(struct dirtyBlockIndex dbiObj) {
	int i,j;
	for(i=0; i<DBI_SIZE; i++) {
		/* Print contents of currently enumerated DBI entry */
		dbiEntryToString(dbiObj.dbi[i]);
		printf("\n");
	}
}

/**
 * Function to allocate memory for the DBI.
 * @param		None.
 * @return		Nothing.
 *
 * Modified by Anthony C. Rivera
 * Created dbiAllocateMemory function by receiving the value dbie
 * as a pointer (which is the same as being sent by reference) 
 * I use -> instead of a period because this is how you reference the
 * members of a structure that is a pointer. Code below is a slight 
 * modification of the code I created in dbiIndex.c.
 *
 * Modified by Zhiyang Ong
 * Change memory space allocation from "unsigned int" to "bool".
 */
void dbiAllocateMemory(struct dirtyBlockIndex *dbie) 
{
	int count;
	// allocate space for dirtyBlockIndexEntry array called dbi
	dbie->dbi = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*DBI_SIZE);    
	 
	for (count = 0; count < DBI_SIZE; count++)
	{
		//allocate space for dirtyBitVector (array) on each struct on the array
		dbie->dbi[count].dirtyBitVector = (bool *)malloc(sizeof(bool)*DBI_GRANULARITY);
	}
	
}






/* ===================================================== */
/* DBI replacement policies. */

/**
 * Function to DBI replacement via the Least Recently
 *	Written (LRW) policy.
 *
 * Sort the DBI entries based on the time that they are last
 *	written to.
 *
 * @param dbiObj	A DBI to run the replacement policy.
 * @param dbie		A DBI entry to replace the LRW entry in DBI.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiLeastRecentlyWritten(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie) {
	/* Index of DBI entry with the least nthWriteOperation. */
	unsigned int indexDBIEntryLeastnthWriteOperation = UINT_MAX;
	/* Current least nthWriteOperation. */
	unsigned int currentLeastnthWriteOperation = UINT_MAX;
	/* nthWriteOperation of currently enumerated DBI entry. */
	int nthWriteOperationInDbiEntry = 0;
	
	/* Index for enumerating dynamic arrays. */
	int i = 0;
	
	/* Enumerate all the DBI entries and get the least nthWriteOperation. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Get nthWriteOperation for DBI entry. */
		nthWriteOperationInDbiEntry = dbiObj.dbi[i].nthWriteOperation;
		/* Is nthWriteOperation < current minimum? */
		if(nthWriteOperationInDbiEntry < currentLeastnthWriteOperation) {
printf("nthWriteOperationInDbiEntry < currentLeastnthWriteOperation:%i < %i.\n",nthWriteOperationInDbiEntry, currentLeastnthWriteOperation);
			/**
			 * Update current minimum and index of the DBI entry with
			 *	least nthWriteOperation with the index of the
			 *	corresponding DBI entry.
			 */
			currentLeastnthWriteOperation = nthWriteOperationInDbiEntry;
			indexDBIEntryLeastnthWriteOperation = i;
		}
	}
	
	/**
	 * Store the row tag of the DBI entry to be evicted, before
	 *	replacing the DBI entry with the least nthWriteOperation.
	 */
	md_addr_t evictedRowTag = dbiObj.dbi[indexDBIEntryLeastnthWriteOperation].rowTag;
	
	/**
	 * Replace the contents of the DBI entry at the index with the
	 *	least nthWriteOperation with the contents of "dbie".
	 */
	dbiObj.dbi[indexDBIEntryLeastnthWriteOperation].validStatus = dbie.validStatus;
	dbiObj.dbi[indexDBIEntryLeastnthWriteOperation].rowTag = dbie.rowTag;
	dbiObj.dbi[indexDBIEntryLeastnthWriteOperation].nthWriteOperation = dbie.nthWriteOperation;
	for(i=0; i<DBI_GRANULARITY; i++) {
		dbiObj.dbi[indexDBIEntryLeastnthWriteOperation].dirtyBitVector[i] = dbie.dirtyBitVector[i];
	}
	
	
	return evictedRowTag;
}





/**
 * Function to DBI replacement via max dirty policy.
 *
 * If there exists multiple DBI entries with the maximum number
 *	of dirty bits, evict and replace the DBI entry among these
 *	that has the lowest index.
 *
 * @param dbiObj	A DBI used to determine the DBI entry with
 *						maximum number of bits.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via max dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMaxDirtyPolicy(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie) {
	/* Index of DBI entry with the maximum number of dirty bits. */
	unsigned int indexDBIEntryMaxNumOfDirtyBits = UINT_MAX;
	/* Current maximum number of dirty bits in enumerated DBI entries. */
	unsigned int currentMaxNumOfDirtyBits = 0;
	/* Number of dirty bits of currently enumerated DBI entry. */
	int numOfDirtyBitsInDbiEntry = 0;
	
	/* Index for enumerating dynamic arrays. */
	int i = 0;
	
	/* Enumerate all the DBI entries and get the maximum. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Get number of dirty bits for DBI entry. */
		numOfDirtyBitsInDbiEntry = getNumberOfDirtyBitsInDBV(dbiObj.dbi[i]);
		/* Is number of dirty bits > current maximum? */
		if(numOfDirtyBitsInDbiEntry > currentMaxNumOfDirtyBits) {
			/**
			 * Update current maximum and index of the DBI entry with
			 *	maximum number of dirty bits with the index of the
			 *	corresponding DBI entry.
			 */
			currentMaxNumOfDirtyBits = numOfDirtyBitsInDbiEntry;
			indexDBIEntryMaxNumOfDirtyBits = i;
		}
	}
	
	/**
	 * Store the row tag of the DBI entry to be evicted, before
	 *	replacing the DBI entry with the maximum number of dirty bits.
	 */
	md_addr_t evictedRowTag = dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].rowTag;
	
	/**
	 * Replace the contents of the DBI entry at the index with the
	 *	maximum number of dirty bits with the contents of "dbie".
	 */
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].validStatus = dbie.validStatus;
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].rowTag = dbie.rowTag;
	dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].nthWriteOperation = dbie.nthWriteOperation;
	for(i=0; i<DBI_GRANULARITY; i++) {
		dbiObj.dbi[indexDBIEntryMaxNumOfDirtyBits].dirtyBitVector[i] = dbie.dirtyBitVector[i];
	}
	
	
	return evictedRowTag;
}







/**
 * Function to DBI replacement via min dirty policy.
 *
 * If there exists multiple DBI entries with the minimum number
 *	of dirty bits, evict and replace the DBI entry among these
 *	that has the lowest index.
 *
 * @param dbiObj	A DBI used to determine the DBI entry with
 *						minimum number of bits.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via min dirty policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t dbiMinDirtyPolicy(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie) {
	/* Index of DBI entry with the minimum number of dirty bits. */
	unsigned int indexDBIEntryMinNumOfDirtyBits = UINT_MAX;
	/* Current minimum number of dirty bits in enumerated DBI entries. */
	unsigned int currentMinNumOfDirtyBits = UINT_MAX;
	/* Number of dirty bits of currently enumerated DBI entry. */
	int numOfDirtyBitsInDbiEntry = 0;
	
	/* Index for enumerating dynamic arrays. */
	int i = 0;
	
	/* Enumerate all the DBI entries and get the minimum. */
	for(i=0; i<DBI_SIZE; i++) {
		/* Get number of dirty bits for DBI entry. */
		numOfDirtyBitsInDbiEntry = getNumberOfDirtyBitsInDBV(dbiObj.dbi[i]);
		/* Is number of dirty bits < current minimum? */
		if(numOfDirtyBitsInDbiEntry < currentMinNumOfDirtyBits) {
			/**
			 * Update current minimum and index of the DBI entry with
			 *	minimum number of dirty bits with the index of the
			 *	corresponding DBI entry.
			 */
			currentMinNumOfDirtyBits = numOfDirtyBitsInDbiEntry;
			indexDBIEntryMinNumOfDirtyBits = i;
		}
	}
	
	/**
	 * Store the row tag of the DBI entry to be evicted, before
	 *	replacing the DBI entry with the minimum number of dirty bits.
	 */
	md_addr_t evictedRowTag = dbiObj.dbi[indexDBIEntryMinNumOfDirtyBits].rowTag;
	
	/**
	 * Replace the contents of the DBI entry at the index with the
	 *	minimum number of dirty bits with the contents of "dbie".
	 */
	dbiObj.dbi[indexDBIEntryMinNumOfDirtyBits].validStatus = dbie.validStatus;
	dbiObj.dbi[indexDBIEntryMinNumOfDirtyBits].rowTag = dbie.rowTag;
	dbiObj.dbi[indexDBIEntryMinNumOfDirtyBits].nthWriteOperation = dbie.nthWriteOperation;
	for(i=0; i<DBI_GRANULARITY; i++) {
		dbiObj.dbi[indexDBIEntryMinNumOfDirtyBits].dirtyBitVector[i] = dbie.dirtyBitVector[i];
	}
	
	
	return evictedRowTag;
}





/**
 * Function for calling the specified DBI replacement policy.
 * @param dbiObj	A DBI that needs an update.
 * @param dbie		A DBI entry to replace the an entry in DBI,
 *						via min dirty policy.
 * @param policyNo	Flag for a selected DBI replacement policy.
 * @return			Row tag for the DBI entry that was replaced.
 */
md_addr_t specifiedDbiReplacementPolicy(struct dirtyBlockIndex dbiObj, struct dirtyBlockIndexEntry dbie, int policyNo) {
	DBI_CACHE_REPLACEMENT_POLICY = policyNo;
printf("DBI_CACHE_REPLACEMENT_POLICY is:%i\n",DBI_CACHE_REPLACEMENT_POLICY);
	/**
	 * Call the specified DBI replacement policy, which is
	 *	specified by the user.
	 * The default 
	 */
	switch(DBI_CACHE_REPLACEMENT_POLICY) {
		/* LRW with bimodal insertion */
/*
		case LRW_W_BIMODAL_INSERTION:
			dbiLRWBimodalInsertion(dbiObj, dbie);
			break;
*/
		/* Rewrite-interval prediction policy */
/*
		case REWRITE_INTERVAL_PREDICTION:
			dbiRewriteIntervalPrediction(dbiObj, dbie);
			break;
*/
		/* Max Dirty - Entry with maximum number of dirty blocks. */
		case MAX_ENTRY:
			dbiMaxDirtyPolicy(dbiObj, dbie);
			break;
		/* Min Dirty - Entry with minimum number of dirty blocks. */
		case MIN_ENTRY:
			dbiMinDirtyPolicy(dbiObj, dbie);
			break;
		default:
			/* Use the DBI Least Recently Written (LRW) policy. */
			dbiLeastRecentlyWritten(dbiObj, dbie);
	}
}















/* ===================================================== */
/* DBI operations. */

/**
 * Function to insert/update metadata in DBI.
 *
 * IMPORTANT ASSUMPTION:
 * Assume that the function to insert/update block in the tag
 *	store will determine the row tag needed for this function.
 *
 *
 * @param dbiObj		A DBI; container of DBI entries which
 *							metadata needs to be modified.
 * @param rTag			Row tag
 * @param dbvIndex		Index to the dirty bit vector
 * @return				Nothing
 */
void modifyMetadataDBI(struct dirtyBlockIndex dbiObj, md_addr_t rTag, unsigned int dbvIndex) {
	/* Boolean flag to indicate if match is found or not. */
	bool rowTagMatch = false;
	/* Index for enumerating dynamic arrays. */
	int i = 0;
	
	/* Enumerate all DBI entries in the DBI*/
	for(i=0; i<DBI_SIZE; i++) {
		/**
		 * Does row tag of currently enumerated BDI entry match
		 *	the given row tag?
		 */
		if(dbiObj.dbi[i].rowTag == rTag) {
			/* Yes. Use dbvIndex to update dirty bit vector. */
			dbiObj.dbi[i].dirtyBitVector[dbvIndex] = true;
			rowTagMatch = true;
		}
	}
	
	/* There does not exist any row match. */
	if(!rowTagMatch) {
		/* Create and insert DBI entry for row containing block. */
		
		/* Create DBI entry to replace the evicted DBI entry. */
		struct dirtyBlockIndexEntry replacementDBI;
		/* Allocate memory to dirty bit vector of replacement DBI entry. */
		replacementDBI.dirtyBitVector = (bool *)malloc(sizeof(bool)*DBI_GRANULARITY);
		replacementDBI.validStatus = CACHE_BLK_VALID;
		replacementDBI.rowTag = rTag;
		for(i=0; i<DBI_GRANULARITY; i++) {
			replacementDBI.dirtyBitVector[i] = false;
		}
		replacementDBI.dirtyBitVector[dbvIndex] = true;
		NUMBER_OF_WRITES_TO_DBI_ENTRIES = NUMBER_OF_WRITES_TO_DBI_ENTRIES + 1;
		replacementDBI.nthWriteOperation = NUMBER_OF_WRITES_TO_DBI_ENTRIES;
		
		/* Is the DBI size full? */
		if(DBI_SIZE > NUM_DBI_ROWS_USED) {
			/**
			 * No. Add this replacement DBI entry.
			 * Copy the contents of the replacement DBI entry
			 *	to the next available DBI row.
			 */
			dbiObj.dbi[NUM_DBI_ROWS_USED].validStatus = replacementDBI.validStatus;
			dbiObj.dbi[NUM_DBI_ROWS_USED].rowTag = replacementDBI.rowTag;
			for(i=0; i<DBI_GRANULARITY; i++) {
				dbiObj.dbi[NUM_DBI_ROWS_USED].dirtyBitVector[i] = replacementDBI.dirtyBitVector[i];
			}
			dbiObj.dbi[NUM_DBI_ROWS_USED].nthWriteOperation = replacementDBI.nthWriteOperation;
			
			
			NUM_DBI_ROWS_USED = NUM_DBI_ROWS_USED + 1;
		}else{
			/**
			 * Call DBI replacement policy to evict a DBI entry.
			 *
			 * Designer's choice:
			 * Since C has no support for throwing exceptions,
			 *	and printing an error statement is superfluously
			 *	addressing the problem, I will consider the case
			 *	of the number of DBI rows used is greater than
			 *	the (maximum) capacity as an error.
			 * However, this error is handled by treating it as
			 *	a full DBI. That is, call DBI replacement policy
			 *	to update the DBI.
			 */
/*			dbiEviction(dbiObj, rTag); */
		}	
	}
}




