%	This is written by Zhiyang Ong as a template for writing reports.

%	The MIT License (MIT)

%	Copyright (c) <2014> <Zhiyang Ong>

%	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

%	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%	Email address: echo "cukj -wb- 23wU4X5M589 TROJANS cqkH wiuz2y 0f Mw Stanford" | awk '{ sub("23wU4X5M589","F.d_c_b. ") sub("Stanford","d0mA1n"); print $5, $2, $8; for (i=1; i<=1; i++) print "6\b"; print $9, $7, $6 }' | sed y/kqcbuHwM62z/gnotrzadqmC/ | tr 'q' ' ' | tr -d [:cntrl:] | tr -d 'ir' | tr y "\n"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Preamble.
\documentclass[letter,12pt]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Importing LaTeX source files, without quoting the ".tex" extension.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	File containing the LaTeX preamble.
\input{./others/preamble_section}
\usepackage{authblk}




% definition of new \LaTeX command for the citation: \cite{Cimatti08} and \cite{Barrett09}
% This allows mathematical/logic symbols to be typeset with the font ``Zapf Chancery'' in ``\LaTeX\ math mode''. To typeset symbols in such font, try: \mathpzc{ABCdef123}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start of document
\begin{document}
\title{Project Report on The Dirty-Block Index}
\date{\today}
\author{Zhiyang Ong \\
	%\href{mailto:ongz@acm.org}{ongz@acm.org} \\
	UIN: 823002514
%	Department of Electrical and Computer Engineering \\
%	Dwight Look College of Engineering\\
%	Texas A\&M University\\
	\and
	Anthony Rivera Carvelli \\
	%\href{mailto:riveracarvelli@tamu.edu}{riveracarvelli@tamu.edu} \\
	UIN: 322006681
%	Department of Computer Science and Engineering \\
}
\maketitle

\tableofcontents

\begin{abstract} 
The performance of cache designs can significantly affect the performance of processors and computer systems \cite{Hennessy2012,Balasubramonian2011,Jacob2008}. The re-organization of the dirty-bit per cache block in each cache into a dirty-block index (DBI) can improve cache performance significantly, while significantly reducing cache area and energy consumption of the memory system \cite{Seshadri2014}. We implemented the architecture and functions of the DBI in SimpleScalar, using an out-of-order, 64-bit Alpha processor as a platform. We implemented different configurations of a DBI-based cache system and different DBI operations. Our implementation of the DBI-based cache system is evaluated by measuring the performance speedup of the simulated {\it Alpha} processor model for different combinations of DBI-based cache system parameters. Lastly, some suggested future work and implementation issues in {\it SimpleScalar} are described.
\end{abstract}








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:Introduction}

The performance of cache designs can significantly affect the performance of processors and computer systems \cite{Hennessy2012,Balasubramonian2011,Patterson2009,Jacob2008,Smith1982}. Cache systems, and the rest of the hierarchical memory system, enable processors to exploit the performance of faster memories, such as register files and first-level caches, and the memory capacity of last-level caches (LLC), main memory, and tertiary storage devices (e.g., disk storage devices). If the performance of the cache system is poor, cache misses and their penalties would significantly slow down the performance of the computer as it needs to access the slower main memory (and, perhaps, tertiary storage) for data and instructions \cite{Hennessy2012}. \\

By removing the dirty bits of the caches from the cache store and reorganizing the cache system of a processor, the performance of an octo-core (i.e., 8-core) processor achieves a speedup of 31\% over a baseline cache design that uses the least recently used (LRU) replacement policy. The reorganized structure of the cache, known as the dirty-block index (DBI), also reduces the area of caches with error detection and correction by 8\% for a 16 MB cache \cite{Seshadri2014}. \\

To obtain a baseline system for evaluating the performance of chip multiprocessors with respect to single-core processors, we implemented a DBI-based cache system for a single-core 64-bit processor based on the {\it Alpha} instruction set architecture (ISA). This DBI-based cache system is implemented with the {\it SimpleScalar} processor simulator \cite{SimpleScalar2004}. The implementations of the DBI-based cache system for chip multiprocessors (i.e., multi- and many- core processors) \cite{Hubner2011,Keckler2009} is beyond the scope of our class project. Carrying out such implementations would complicate verification and validation of our cache system implementations. \\

The goal of this report is to document what we have done for the class project: we have implemented a DBI-based last-level cache (LLC) for a single-core 64-bit {\it Alpha} processor model, using {\it SimpleScalar}. \\

The rest of this paper is described is organized as follows. In Subsection \S\ref{ssec:Motivation}, we state the motivation for implementing a DBI-based LLC for our class project. Next, in Subsection \S\ref{ssec:BackgroundInformation}, we describe some basic concepts that the DBI-based LLC is based on. Section \S\ref{sec:RelatedWork} discusses relevant work to this project. Subsequently, in Section \S\ref{sec:DirtyBlockIndexStructure}, we describe the structure of the DBI. Following that, in Sections \S\ref{sec:DirtyBlockIndexOperations} and \S\ref{ssec:DirtyBlockIndexReplacementPolicies}, we discuss the DBI operations and DBI replacement policies. In Sections \S\ref{sec:PerformanceAnalysisMethodology} and \S\ref{sec:ExperimentalResultsandDiscussion}, we describe our performance evaluation methodology and experimental progress. Furthermore, in Section \S\ref{sec:FutureWork}, we discuss future work that can be done. Section \S\ref{sec:Conclusions} draws some preliminary conclusions on the project. Finally, in the Appendix, we describe the division of labor in \S\ref{sec:AppendixADivisionofLabor} and implementation issues in \S\ref{sec:AppendixBImplementationIssuesinSimpleScalar}.


%	sec:DirtyBlockIndexOptimizations
%Lastly, in Section \S\ref{sec:ProposedExperimentalMethodology}, we propose an experimental methodology for evaluating our implementation of the DBI-based cache system.












%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Motivation}
\label{ssec:Motivation}

%With this project we are looking to expand our knowledge on the subject of cache design and optimization by implementing a new method to improve cache performance. 
%The {Dirty-Block} Index is a structure that stores a DRAM row and marks which blocks in that row are dirty. 
%Thereby taking the dirty bits out of the cache tag store and on a single entry grouping the dirty bits of the all the blocks of the same DRAM row together on one entry. 
%There are three dirty bit cache optimizations which their efficient implementations under DBI are used to prove its effectiveness. 
%The optimizations mentioned are Efficient {DRAM-Aware} Aggressive Writeback, Efficient Cache Lookup Bypass and Reducing ECC Overhead \cite{Seshadri2014}. 
%DBI makes the implementation of these optimizations simpler and more efficient while also making it possible to implement all three at the same time without further increasing complexity \cite{Seshadri2014}. 
%We want to implement the DBI along with the optimizations mentioned to see if we can replicate the results for a single core processor, validate the claims mentioned on the paper and perform an area and power analysis. 
%Also we want to test how DBI design choices like size, granularity and replacement policies affect our results. 

%To address the ``memory wall'' challenge \cite{Hennessy2012,Wulf1995}, we want to design a cache system for a single-core processor with faster cache performance and is more energy-efficient. The cache re-organization of the dirty-block index lead to better performance and energy consumption over traditional dirty bit implementations, and has a smaller area on silicon \cite{Seshadri2014}. Since the DBI is implemented on a single-core processor and chip multiprocessors (CMPs), we have chosen to limit our scope of the class project to single-core processor implementation and evaluation of DBI and associated techniques. This will help us gain an understanding of how much speedup can be obtained by the computer system by evaluating its performance speedup on industry-grade benchmarks. Once we have successfully implemented the DBI-based cache design single-core processor, we can extend the implementation for CMP systems as future work \cite{Hennessy2012,Shen2005a,Culler1999}. This helps us to run experiments faster, and complete the project in a timely manner.
To address the ``memory wall'' challenge \cite{Hennessy2012,Wulf1995}, we want to design a cache system for a single-core processor that has faster cache performance and is more energy-efficient. The cache re-organization of the dirty-block index lead to better performance and energy consumption over traditional dirty bit implementations, and has a smaller area on silicon \cite{Seshadri2014}. Since the DBI is implemented on a single-core processor and chip multiprocessors (CMPs), we have chosen to limit our scope of the class project to implementations and evaluation of DBI-based techniques for a single-core processor. This will help us gain an understanding of how much speedup can be obtained by the computer system by evaluating its performance speedup on industry-grade benchmarks. Once we have successfully implemented the DBI-based cache design for a single-core processor, we can extend the implementation for CMP systems as future work \cite{Hennessy2012,Shen2005a,Culler1999}. This helps us to run experiments faster, and complete the project in a timely manner.







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Background Information}
\label{ssec:BackgroundInformation}

%	Background information for the DBI-based cache system design is provided as follows.
%	Describe what the dirty bit is for.
Processors with cache systems use write policies, such as write-through and write-back (or copyback), to write data intro the memory \cite{Null2012,Stallings2010,Patterson2009}. Write-back caches are more popular than write-through caches because they require less bandwidth for write operations \cite{Shen2005a}; write-back caches are also faster than write-through caches \cite{Null2012}. Hence, the wiring complexity of the on-chip communication network \cite{Ho2001} and associated noise coupling \cite{Cogez2011,Dally2001} is simpler with write-back caches than write-through caches. For a write-back cache, a processor uses the dirty bit to determine if a block on the cache line has been modified such that it has data values that can be inconsistent with those of lower levels caches and the main memory \cite{Hennessy2012,Shen2005a,Dandamudi2003,Smith1982}. \\

%	Cache line: Column of memory device/SRAM
%	Cache row: DBI, used with rows rather than blocks. A row has multiple blocks.
%	13,14,6,11,15,2,16
%	Shen2005a, pp. 122. Each cache line as a valid bit and a dirty bit... dirty bit is used in write-back cache.
%	MIPS (See MIPS Run.pdf), write-back cache: 67-68.76. Dirty bits and pages 145, dirty lives 67, 361. 2nd edition: write-back championship and dirty, pp. 84, 87, 90, 93, 437, 439.
%	Null & Labor, pp. 249, copyback and standard definition.
%	Dumas / DumasII2006, pp. 73 definition. 0 -> clean. 1 -> dirty. \S7.2
%	P&H, 3E: \S7.2 483--484, Don't confuse dirty bits of caches, TLB, and VM (virtual memory). P&H, 4E: pp. 467--468.
%	H&P, 5E: MESI and MOESI are for chip multiprocessors. B1--B11. Require less memory interconnect, save power. D-61 to D-64.
%	Dirty bits: 145, 67, 361, 145
%	Dirty bits are used in write-back caches in single-core processors
%	For a cache line (write-back policy, or copy-back policy), it has a valid bit and a dirty bit. The dirty bit is used to indicate that the dirty bit on the cache line (verify with other references: is it cache line or block, or otherwise).
%	Dandamudi2003, Chapter 17. pp. 714 Problem: There exists two copies of data: main memory copy and cached copy...	Improve performance of write-back cache (pp. 715). Information of...	Fundamentals of computer organization and design
%	Stallings2010. 8E, pp. 136-137 write policies (write-through and write-back), pp.137 definition of dirty bit in write-back caches... 

Due to the aforementioned advantages of the write-back cache over write-through cache, we shall focus on optimizing the cache organization of write-back cache. The comparison of cache optimization for each write policy is beyond the scope of this class project.
%the study of cache optimization of caches with other write policy

%	Jacbob2008: pp.47,70 (definition) dirty bit. 908, 733
%	techniques for SRAM design











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}
\label{sec:RelatedWork}

To the best knowledge of the authors, this is the first paper that modifies the organization of the dirty bit index \cite{Seshadri2014}. Prior techniques \cite{Jaleel2008,Lee2010,Qureshi2007,K2012,Stuecheli2010} for optimizing cache performance regarding dirty bit index can be considered for benchmarking our DBI implementation with existing work.

%	Key references: 27, 44, 51.
%	Or, 18, 27, 42, 44, 51
%	Look at reference [33]
%	determination of whether a block is dirty 33, 44, 49
%	identification of dirty blocks in a given spatial locality 27, 47, 51
%	23, 27, 29, 30, 33, 44, 51, 58, 59




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dirty-Block Index Structure}
\label{sec:DirtyBlockIndexStructure}


\begin{figure}[h]
\centering 
\includegraphics[width=6in]{./pics/conventional-cache-tag-store}
\caption{Structure of a conventional cache, with storage for its cache tag \cite{Seshadri2014}.}
\label{fig:conventionalcachetagstore}
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[width=6in]{./pics/cache-tag-store-w-dbi}
\caption{Structure of a last-level cache with a dirty-block index \cite{Seshadri2014}.}
\label{fig:cachetagstorewdbi}
\end{figure}


Figure \ref{fig:conventionalcachetagstore} shows a conventional cache with storage for its cache block tags. When the dirty bit in the cache tag per cache block is re-structured into a dirty-block index with a dirty-bit vector, it shifts the storage of the dirty bit from each cache block to each cache row. Here, the dirty bit vector refers to a collection of dirty bit vectors for the cache row; each dirt bit in the vector indicates whether a cache block is dirty. This DBI reorganization is shown in Figure \ref{fig:cachetagstorewdbi}. \\


Some parameters used to describe the DBI structure are listed as follows: \vspace{-0.3cm}
\begin{enumerate} \itemsep -4pt
\item DBI size = number of entries/rows in the DBI = number of sets in {\it SimpleScalar} = index of set associative cache
\item DBi granularity = size of the dirty bit vector in the DBI = associativity of the cache that determines which cache line (i.e., column) is used to access a cache block
\end{enumerate}

\begin{figure}[h]
\centering 
\includegraphics[width=6in]{./pics/granularity2}
\caption{Structure of a last-level cache with a dirty-block index \cite{Seshadri2014}.}
\label{fig:granularity2}
\end{figure}

\begin{figure}[h]
\centering 
\includegraphics[width=6in]{./pics/granularity}
\caption{Structure of a last-level cache with a dirty-block index \cite{Seshadri2014}.}
\label{fig:granularity}
\end{figure}

An illustration of the granularity of the DBI is shown in Figure \ref{fig:granularity2}. It refers to how we can partition each cache row into DBI entries. For a granularity of one, each cache row is represented by a DBI entry. However, for a DBI granularity of 0.5, each cache row is represented by two DBI entries. Consequently, for a DBI granularity of 0.25, each cache row is represented by four DBI entries. In Figure \ref{fig:granularity}, we illustrate the contents of a conventional cache tag store and a DBI with a granularity of two.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dirty-Block Index Operations}
\label{sec:DirtyBlockIndexOperations}

A sequential ordering of DBI operations is listed as follows: 1) read access to the cache; 2) writeback request to the cache; 3) cache eviction; and 4) DBI eviction; see Figure \ref{fig:dbioperations}. The DBI addition to the LLC does not affect the read access operation. \\

For the writeback request to the cache, two sub-operations are performed. The first suboperation is described as follows. If the cache block is not in the cache, it will be inserted into the cache. Else, if the block is already in the cache, its corresponding contents in the data store, which is not shown, is updated. As for the second suboperation, the cache would update the DBI to reflect that the cache block is dirty. If the DBI has a DBI entry for the cache row containing that cache block, the corresponding bit in the dirty bit vector of that DBI entry is set to one. Else, a new DBI entry is created and inserted into the DBI with the corresponding bit in the dirty bit vector of that DBI entry is set to one. Here, whenever a new DBI entry is inserted into the DBI, if the DBI has reached its capacity (maximum size), it needs to evict a DBI entry and replace that with the new DBI entry. This is known as DBI eviction, which is the last step. DBI eviction is carried out via a DBI replacement policy, which is described in Section \ref{ssec:DirtyBlockIndexReplacementPolicies}.

As for cache eviction, when a cache block is evicted from the cache, it is written back to main memory. This requires the cache to check with the DBI whether the cache block is dirty. If the cache block is dirty, the cache will generate a writeback request to the memory controller and update the DBI to reflect that that block is no longer dirty (i.e., reset that bit). If no other blocks in the DBI entry are dirty, invalidate that entry.


\begin{figure}[h]
\centering 
\includegraphics[width=6in]{./pics/dbi-operations}
\caption{Sequential ordering of DBI operations: 1) read access to the cache; 2) write back request to the cache; 3) cache eviction; and 4) DBI eviction \cite{Seshadri2014}.}
\label{fig:dbioperations}
\end{figure}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dirty-Block Index Replacement Policies}
\label{ssec:DirtyBlockIndexReplacementPolicies}

%    \begin{codebox}
%    \Procname{$\proc{NAME OF THE ALGORITHM}({\it ARGUMENTS})$}
%    \label{lst:LABEL}
%    \zi \Comment {\it Input ARGUMENT \#1: Definition}
%    \zi \Comment {\it Input ARGUMENT \#2: Definition}
%    \li BODY OF THE PROCEDURE
%    \zi	\Comment {\it What is the output of this procedure?}
%    \li	\Return%    \end{codebox}



We have implemented three DBI replacement policies: least recently written (LRW); Max-Dirty, and Min-Dirty. LRW policy replaces the DBI entry that has the longest time period since it was last written to. Max-Dirty replaces the DBI entry with the most number of dirty bits in the dirty bit vector. On the other hand, Min-Dirty replaces the DBI entry with the most number of dirty bits in the dirty bit vector.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Dirty-Block Index-enabled Optimizations}
%\label{sec:DirtyBlockIndexOptimizations}

















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Performance Analysis Methodology}
\label{sec:PerformanceAnalysisMethodology}

Our performance analysis methodology is described as follows. Firstly, we obtained the baseline performance of an {\it Alpha} processor, by simulating it with the {\it SimpleScalar} processor simulator. {\it Alpha} binaries for the {\it SPEC CPU 2000} \cite{Kim2014b,SPEC1999} benchmark suite are used as benchmarks for performance evaluation. We used the 2000 version of the {\it SPEC CPU} benchmark suite, since we have yet to figure out how to use the 2006 version \cite{SPEC2014} effectively. \\

We implemented the DBI structure and its operations by extending the processor simulator {\it SimpleScalar}. Next, performance evaluation of our modified {\it Alpha} processor with DBI is carried out for various configurations of DBI parameters (or design choices), such as DBI size, DBI granularity, and DBI replacement policy; see Table \ref{tab:DBIconfiguration} for the details of the considered configuration of DBI parameters and {\it SimpleScalar}. It is benchmarked against a baseline {\it Alpha} processor without DBI. \\

The metrics that we used to evaluate our DBI implementation in an {\it Alpha} processor are: instructions per cycle, write row hit rate, last level cache tags lookup per kilo instructions, memory writes per kilo instructions, and read row hit rate. Measurements of these metrics are obtained by simulation (using {\it SimpleScalar}) to determine how each configuration performs on each benchmark, and the geometric average of its performance on the benchmark suite. We analyze the experimental results statistically and discuss our experimental findings in Section \ref{sec:ExperimentalResultsandDiscussion}. We also determine the fidelity of our simulation results with respect to the published experimental results \cite{Seshadri2014}. \\


\begin{table}[htdp]
%\caption{Configuration of the Dirty-Block Index's parameters. {\Huge Fix THIS!!!}}	\vspace{-0.2in}
\caption{Configuration of the Dirty-Block Index's parameters. }	\vspace{-0.2in}
\label{tab:DBIconfiguration}
	\begin{center}
		\begin{tabular}{|c|c|}
		\hline
		Design Parameter & Value of Specification  \\
		\hline
		Processor ISA & {\it Alpha}  \\
		\hline
		Levels of cache & 2  \\
		\hline
		DBI size & 1, 2, 4 MB \\
		\hline
		DBI granularity & 1, 0.5, 0.25, 0.125 of the capacity of a cache row \\
		\hline
		Cache architecture & Fully unified cache hierarchy  \\
		\hline
		DBI Replacement Policies & Least recently written (LRW) policy \\
		 & Max-Dirty policy \\
		 & Min-Dirty policy \\ 
		\hline
		\end{tabular}
	\end{center}
\end{table}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Results and Discussion}
\label{sec:ExperimentalResultsandDiscussion}

We are still currently debugging our implementation of DBI operations. When we run our implementation of DBI-based LLC on {\it SimpleScalar}, it produces segmentation faults for the following four {\it SPEC CPU 2000} benchmarks: {\tt bzip2}, {\tt crafty}, {\tt parser}, and {\tt mcc}.






















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Future Work}
\label{sec:FutureWork}

In this section, we describe how our work can be extended as follows. \\

Firstly, we can complete implementing all the algorithms and techniques mentioned in the paper for a complete evaluation of the DBI solution to improve performance, reduce cache area, and decrease net energy consumption by the memory system. This includes implementing the following DBI replacement policies {\it LRW with Bimodal Insertion Policy} \cite{Qureshi2007} and {\it rewrite-interval prediction policy} \cite{Jaleel2008}. We can also model the cache with a dynamic random-access memory (DRAM) \cite{MicronTechnology2014}, instead of a static random-access memory (SRAM) device. In addition, the DBI-enabled optimization techniques can be implemented. They include the following \cite{Seshadri2014}: \vspace{-0.3cm}
\begin{enumerate} \itemsep -4pt
\item Efficient DRAM-aware aggressive write back
\item Efficient cache lookup bypass
\item Reducing ECC overhead
\item Load balancing memory access
\item Fast lookup for dirty status
\item Cache flushing
\item Direct memory access (DMA)
\item Metadata about dirty blocks
\end{enumerate}

Next, the DBI structure can be implemented for performance evaluation of a chip multiprocessor (CMP) system \cite{Olukotun2007}. This can be done by modeling the dirty-block index in the shared last-level cache (LLC) of a multi-core {\it Alpha}, {\it ARM}, {\it SPARC}, or {\it x86} processor, using the {\it gem5} processor simulator \cite{Binkert2011,gem5developers2014}. Subsequently, the dirty-block index and other performance improvement techniques can be explored under a design space exploration framework for multi-objective optimization \cite{Bailey2010a,Bailey2007,Kempf2011,Leupers2010,Monchiero2008,Shen2005a,Vachharajani2004}. This would provide a baseline for further experimental work on CMP systems. \\

Lastly, pre-silicon validation of the processor (i.e., feasibility analysis of DBI-based LLC implementations as VLSI systems) and floorplanning of the {\it Alpha} processor with DBI-based LLC can be investigated to get a more detailed area analysis of the hardware resource usage/floorplan. In addition, power analysis of the LLC can be carried out with {\it CACTI 6.0} \cite{Muralimanohar2007}.









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\label{sec:Conclusions}

We are still currently debugging our implementation of DBI operations. When we run our implementation of DBI-based LLC on {\it SimpleScalar}, it produces segmentation faults for the following four {\it SPEC CPU 2000} benchmarks: {\tt bzip2}, {\tt crafty}, {\tt parser}, and {\tt mcc}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Acknowledgments}
\label{sec:Acknowledgments}

%The authors are extremely grateful to Prof. Eun-Jung Kim for suggestions of possible topics to work on for the class project, as well as advice regarding project management and experimental methodology for our class project. Also, Zhiyang Ong would like to thank Mr. Eric Chaidez, Mr. Kexin Chen, Mr. Amey Parulkar, and Mr. Paul V. Akshay for preliminary discussions of possible topics to work on for the class project. In addition, Zhiyang Ong would like to thank Prof. Paul V. Gratz for suggesting that we look at pre-silicon validation for our cache design, and whether it is feasible for implementation in silicon.
The authors are extremely grateful to Prof. Eun-Jung Kim for suggestions of possible topics to work on for the class project, as well as advice regarding project management and experimental methodology for our class project. They are also thankful to Mr. Kyung-Hoon Kim for providing them with access to the {\it SPEC CPU 2006} benchmarks. Also, Zhiyang Ong would like to thank Mr. Eric Chaidez, Mr. Kexin Chen, Mr. Amey Parulkar, and Mr. Velpula Akshay Paul for preliminary discussions of possible topics to work on for the class project. In addition, Zhiyang Ong would like to thank Prof. Paul V. Gratz for suggesting that we look at pre-silicon validation for our cache design, and determine whether it is feasible for implementation in silicon. Lastly, Zhiyang Ong is grateful to Mr. Jiayi Huang and other classmates for their encouragement and suggestions.



















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Appendix A: Division of Labor}
\label{sec:AppendixADivisionofLabor}
%Appendix


In Appendix A, we describe the tasks that each of us did. \\

The tasks that Zhiyang Ong completed are: \vspace{-0.3cm}
\begin{enumerate} \itemsep -4pt
\item Developed test suites/scripts to test the code for: \vspace{-0.3cm}
	\begin{enumerate} \itemsep -2pt
	\item The {\tt struct} representing a DBI entry, {\it dirtyBlockIndexEntry}. The test script allocates memory for a DBI (and the dirty bit vector for each DBI entry), which is represented as a dynamic array of DBI entries, creates a list of {\it dirtyBlockIndexEntry}s and inserts them into the DBI, and read the contents of the test script.
	\item The {\tt struct} representing the DBI, {\it dirtyBlockIndex}. The test script initializes a DBI with DBI entries filled to capacity. Next, a DBI entry is added to the DBI via evicting a DBI entry. This is tested on all three implemented DBI replacement policies. 
	\item {\it Makefile} for automated regression test automation
	\end{enumerate}
\item Developed the {\tt struct} {\it dirtyBlockIndex}, which include functions to: \vspace{-0.3cm}
	\begin{enumerate} \itemsep -2pt
	\item Allocate memory for the DBI
	\item Print the contents of the DBI
	\item To carry out DBI replacement policies: \vspace{-0.2cm}
		\begin{enumerate} \itemsep -2pt
		\item Least recently written (LRW) policy, which evicts the DBI entry that is the least recently written to
		\item Max-Dirty policy, which evicts the DBI entry with the maximum number of dirty bits
		\item Min-Dirty policy, which evicts the DBI entry with the minimum number of dirty bits
		\end{enumerate}
	\item (A generic function) To switch between different DBI replacement policies, depending on the configuration options specified by the user in the file {\tt dbiEntry.h}
	\item Modify the metadata in DBI (i.e., update the dirty bit vector of {\it dirtyBlockIndexEntry})
	\item 
	\end{enumerate}
\item Modified the {\tt struct} {\it dirtyBlockIndexEntry}: \vspace{-0.3cm}
	\begin{enumerate} \itemsep -2pt
	\item To include variables for supporting DBI replacement policies, such as {\it Least Recently Written}
	\item To include functions: \vspace{-0.2cm}
		\begin{enumerate} \itemsep -2pt
		\item To print the contents of the DBI entry, {\it dirtyBlockIndexEntry}
		\item To determine the number of dirty bits in the dirty bit vector of {\it dirtyBlockIndexEntry}
		\end{enumerate}
	\end{enumerate}
\item Wrote most of the project proposal
\item Wrote most of the project report
\item Created most of the presentation slides for the class project presentation
\end{enumerate}


These are the tasks done by Anthony: \vspace{-0.3cm}
\begin{enumerate} \itemsep -4pt
\item Created dbiIndex.h and dbiIndex.c which were precursors to dbi.h, dbi.c, dbiEntry.h, dbiEntry.c,
\item Wrote dbiAllocateMemory function on dbi.c.
\item Implemented DBI for DL2 cache and DL2 unified cache
\item Modified cache.h and cache.c so that cache writes would be reflected on the DBI Dirty Bit Vector
\item Figured out how to pass the DBI structure as a reference (pass it as a pointer)
\item Modified sim-outorder.c to create a DBI when a L2 cache is created and to take its row size from set size and dirty bit vector size from associativity size
\item Modified sim-cache.c to create a DBI when a l2 cache is created and to take its row size from set size and dirty bit vector size from associativity size
\item Figured out that dbi row size is taken from sets and dirty bit vector size is taken from associativity and modified dbi.c so that dbiAllocateMemory 
\item Helped with the design of the DBI implementation along with Mr. Ong.
\end{enumerate}

Modifications and additions to the source code made by Zhiyang Ong were usually tagged with {\it ``Modified by Zhiyang Ong''} when committing the code to the repository. \\


We note that the amount of tasks completed does not necessarily reflect the amount of work done by each individual. This is because some of the completed tasks are fairly repetitive and/or simpler while other tasks require a lot more work.







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Appendix B: Implementation Issues in {\it SimpleScalar}}
\label{sec:AppendixBImplementationIssuesinSimpleScalar}


In this Appendix, we discuss some implementation issues regarding the C code that we have implemented and modified. \\

Firstly, we have decided not to implement the feature for configuring the parameters (DBI size, DBI granularity, and DBI replacement policy) via the option/flag in the command line. This is because we wanted to save some development time implementing and testing the code to support this feature. To enable these features to be configured, we have used {\it C} macros and static variables in the file {\tt dbiEntry.h} to set the parameters for the DBI organization. By placing all the parameters for configuration in the file {\tt dbiEntry.h}, it is easier to configure the DBI for another DBI configuration in one file/location. \\

We have chosen to implement the DBI as a dynamic array, so that we do not have to implement a linear data structure (such as a linked list) to implement the DBI. It is defined in {\tt dbi.h} and has its functions implemented in {\tt dbi.c}. This keeps the implementation of the DBI separate from the cache. Due to separation of concerns, this enables unit testing and supports automated regression testing of the DBI implementation, before we integrate the code of the DBI and DBI entry into the code base of {\it SimpleScalar}. \\

In the implementation of the LRW DBI replacement policy, we are aware that using integers, instead of a short bit vector, to represent the last {\tt write} operation on a given DBI entry is more costly in hardware than the bit vector. However, using the {\tt integer} type enables easier and simpler comparison of the number representing the last {\tt write} operation with this metric of other {\tt DBI entries} in the {\it C} implementation. \\

We did not implement a L3 cache due to the perceived complexity of the implementation in {\it SimpleScalar}. In an effort to demonstrate a working version of DBI on the LLC, we have decided to implement the DBI on the fully unified L2 cache of {\it SimpleScalar}. \\

The repository for our report and source code is available on {\it Bitbucket} at: \url{https://bitbucket.org/eda_globetrotter/lecce-arch}.











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	End of document
%
%	Inserting references
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Beginning of BACK MATTER: bibliography, indexes and colophon
%\backmatter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{\linespread{1}
\bibliographystyle{plain}
%\bibliography{./references/references}
\bibliography{/data/research/antipastobibtex/references}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}