#include "dbi.h"
#include <stdio.h>

int main()
{

	struct dirtyBlockIndex dbiTest;

	int dbiSize = 50;
	int dbiGranularity = 50;
	int count;
	
	// allocate space for dirtyBlockIndexEntry array called dbi
	dbiTest.dbi = (struct dirtyBlockIndexEntry *)malloc(sizeof(struct dirtyBlockIndexEntry)*dbiSize);    
	 
	for (count = 0; count < dbiSize; count++)
	{
		//allocate space for dirtyBitVector (array) on each struct on the array
		dbiTest.dbi[count].dirtyBitVector = (unsigned int *)malloc(sizeof(unsigned int)*dbiGranularity);
	} 

}
